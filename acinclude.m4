# FORS_SET_PREFIX(PREFIX)
#-------------------------
AC_DEFUN([FORS_SET_SEX_PREFIX],
[
    if test "x$exec_prefix" != "xNONE"; then
        sex_exec_prefix=$exec_prefix/lib/${PACKAGE}-${VERSION}/bin
        ac_configure_args="$ac_configure_args SEXBINPATH=$sex_exec_prefix"
    elif test "x$prefix" != "xNONE"; then
        sex_exec_prefix=$prefix/lib/${PACKAGE}-${VERSION}/bin
        ac_configure_args="$ac_configure_args SEXBINPATH=$sex_exec_prefix"
    else
        sex_exec_prefix=$ac_default_prefix/lib/${PACKAGE}-${VERSION}/bin
        ac_configure_args="$ac_configure_args SEXBINPATH=$sex_exec_prefix"
    fi
])

# FORS_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#--------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([FORS_SET_VERSION_INFO],
[
    fors_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    fors_major_version=`echo "$fors_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    fors_minor_version=`echo "$fors_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    fors_micro_version=`echo "$fors_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$fors_major_version"; then
        fors_major_version=0
    fi

    if test -z "$fors_minor_version"; then
        fors_minor_version=0
    fi

    if test -z "$fors_micro_version"; then
        fors_micro_version=0
    fi

    FORS_VERSION="$fors_version"
    FORS_MAJOR_VERSION=$fors_major_version
    FORS_MINOR_VERSION=$fors_minor_version
    FORS_MICRO_VERSION=$fors_micro_version

    if test -z "$4"; then
        FORS_INTERFACE_AGE=0
    else
        FORS_INTERFACE_AGE="$4"
    fi

    FORS_BINARY_AGE=`expr 100 '*' $FORS_MINOR_VERSION + $FORS_MICRO_VERSION`
    FORS_BINARY_VERSION=`expr 10000 '*' $FORS_MAJOR_VERSION + \
                          $FORS_BINARY_AGE`

    AC_SUBST(FORS_VERSION)
    AC_SUBST(FORS_MAJOR_VERSION)
    AC_SUBST(FORS_MINOR_VERSION)
    AC_SUBST(FORS_MICRO_VERSION)
    AC_SUBST(FORS_INTERFACE_AGE)
    AC_SUBST(FORS_BINARY_VERSION)
    AC_SUBST(FORS_BINARY_AGE)

    AC_DEFINE_UNQUOTED(FORS_MAJOR_VERSION, $FORS_MAJOR_VERSION,
                       [FORS pipeline major version number])
    AC_DEFINE_UNQUOTED(FORS_MINOR_VERSION, $FORS_MINOR_VERSION,
                       [FORS pipeline minor version number])
    AC_DEFINE_UNQUOTED(FORS_MICRO_VERSION, $FORS_MICRO_VERSION,
                       [FORS pipeline micro version number])
    AC_DEFINE_UNQUOTED(FORS_INTERFACE_AGE, $FORS_INTERFACE_AGE,
                       [FORS pipeline interface age])
    AC_DEFINE_UNQUOTED(FORS_BINARY_VERSION, $FORS_BINARY_VERSION,
                       [FORS pipeline binary version number])
    AC_DEFINE_UNQUOTED(FORS_BINARY_AGE, $FORS_BINARY_AGE,
                       [FORS pipeline binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# FORS_SET_PATHS
#----------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([FORS_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/'
    fi

    htmldir='${pipedocsdir}/html'

#    if test -z "$htmldir"; then
#        htmldir='${datadir}/doc/${PACKAGE}/html'
#    fi

    if test -z "$configdir"; then
       configdir='${prefix}/share/esopipes/${PACKAGE}-${VERSION}/config'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(htmldir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)



    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(FORS_PLUGIN_DIR,"esopipes-plugins", 
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(FORS_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

    # Define the preprocessor symbols for the sextractor executable
    # and the configuration files.

    eval sext_bindir="${prefix}/lib/${PACKAGE}-${VERSION}/bin"

    AC_DEFINE_UNQUOTED(FORS_SEXTRACTOR_PATH, "$sext_bindir",
                       [Absolute path to the sextractor executable])

    AC_SUBST(sext_bindir)

    eval d="$configdir"
    eval sext_configdir="$d"

    AC_DEFINE_UNQUOTED(FORS_SEXTRACTOR_CONFIG, "$sext_configdir",
                       [Absolute path to the sextractor configuration files])

])


# FORS_CREATE_SYMBOLS
#---------------------
# Define include and library related makefile symbols
AC_DEFUN([FORS_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    FORS_INCLUDES='-I$(top_srcdir)/fors'
    FORS_LDFLAGS='-L$(top_builddir)/fors'
    IRPLIB_INCLUDES='-I$(top_srcdir)/irplib'
    MOSCA_INCLUDES='-I$(top_srcdir)/mosca/libmosca'

    # No -L for IRPLIB which is statically linked

    #WCSLIB_LDFLAGS='-L$(top_builddir)/wcslib-4.2/C'
    #WCSLIB_INCLUDES='-I$(top_srcdir)/wcslib-4.2/C'

    # Library aliases

    LIBFORS='$(top_builddir)/fors/libfors.la'
    #LIBWCSLIB='$(top_builddir)/wcslib-4.2/C/libwcs-4.2.a'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'
    LIBMOSCA='$(top_builddir)/mosca/libmosca/libmosca.la'


    # Substitute the defined symbols

    AC_SUBST(FORS_INCLUDES)
    AC_SUBST(FORS_LDFLAGS)
    AC_SUBST(LIBFORS)

    #AC_SUBST(WCSLIB_INCLUDES)
    #AC_SUBST(WCSLIB_LDFLAGS)
    #AC_SUBST(LIBWCSLIB)

    AC_SUBST(IRPLIB_INCLUDES)
    AC_SUBST(LIBIRPLIB)

    AC_SUBST(MOSCA_INCLUDES)
    AC_SUBST(LIBMOSCA)


    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(FORS_INCLUDES)  $(MOSCA_INCLUDES) $(CPL_INCLUDES) $(CX_INCLUDES) $(IRPLIB_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(FORS_LDFLAGS) $(CPL_LDFLAGS) $(CX_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)

])
