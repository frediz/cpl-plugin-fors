#!/usr/bin/make -f
# -*- makefile -*-
#export DH_VERBOSE=1

include  /usr/share/dpkg/pkg-info.mk

VERSION ?= $(shell echo '$(DEB_VERSION)' | sed -e 's/^[[:digit:]]*://' -e 's/[-].*//' -e 's/+.*//')
PIPELINE ?= $(shell echo '$(DEB_SOURCE)' | sed -e 's/cpl-plugin-//')

get-orig-source:
	sh ./debian/repackage.sh

%:
	dh  $@ --with sphinxdoc

debian_files:
	if [ -d calib/cal ] ; then \
	    dfiles=".install -doc.manpages -doc.docs -doc.doc-base -calib.install" ; \
	else \
	    cp -f debian/README.Debian.in debian/README.Debian ; \
	    dfiles=".install -doc.manpages -doc.docs -doc.doc-base -calib.postinst -calib.prerm -calib.lintian-overrides" ; \
	fi ; \
	for f in $$dfiles ; do \
	    sed "s/__VERSION__/$(VERSION)/g;s/__PIPELINE__/${PIPELINE}/g" \
	        < debian/cpl-plugin$$f.in \
                > debian/${DEB_SOURCE}$$f ; \
	done

override_dh_install: debian_files
	dh_install

override_dh_installman-indep:
	mkdir -p man
	python3 debian/create_manpage.py ${PIPELINE}
	dh_installman

override_dh_installdocs-indep:
	mkdir -p sphinx
	python3 debian/create_sphinx.py ${PIPELINE}
	sphinx-build sphinx sphinx/html
	dh_installdocs

override_dh_sphinxdoc:
	if [ -d sphinx ] ; then \
		dh_sphinxdoc ; \
	fi

override_dh_clean:
	dh_clean
	rm -rf debian/${DEB_SOURCE}.install \
               debian/${DEB_SOURCE}-doc.manpages \
               debian/${DEB_SOURCE}-doc.docs \
               debian/${DEB_SOURCE}-doc.doc-base \
	       debian/${DEB_SOURCE}-calib.lintian-overrides \
	       debian/${DEB_SOURCE}-calib.install \
	       debian/${DEB_SOURCE}-calib.postinst \
               debian/${DEB_SOURCE}-calib.prerm \
               debian/README.Debian \
	       man sphinx

override_dh_auto_configure:
	dh_auto_configure -- --prefix=/usr/

override_dh_makeshlibs:
	sed -i "/dependency_libs/ s/'.*'/''/" `find . -name '*.la'`

override_dh_installchangelogs:
	if [ -s ChangeLog ] ; then \
	  dh_installchangelogs ChangeLog ; \
	else \
	  dh_installchangelogs ; \
	fi

override_dh_fixperms:
	dh_fixperms
	if [ -d debian/cpl-plugin-fors-calib/usr/share/cpl-plugins/ ] ; then \
	    chmod 644 debian/cpl-plugin-fors-calib/usr/share/cpl-plugins/*/cal/*.fits ; \
	fi

override_dh_gencontrol-indep:
	dh_gencontrol
	if [ ! -d calib/cal ] ; then \
	  sed "s/Installed-Size:.*/Installed-Size: $(shell cat calib/cal_size)/" -i debian/${DEB_SOURCE}-calib/DEBIAN/control ; \
	fi

override_dh_auto_test:
	VERBOSE=1 dh_auto_test
