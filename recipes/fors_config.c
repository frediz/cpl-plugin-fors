/* $Id: fors_config.c,v 1.5 2013-10-09 15:59:38 cgarcia Exp $
 *
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-10-09 15:59:38 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <math.h>
#include <cpl.h>
#include <moses.h>
#include <fors_dfs.h>

static int fors_config_create(cpl_plugin *);
static int fors_config_exec(cpl_plugin *);
static int fors_config_destroy(cpl_plugin *);
static int fors_config(cpl_parameterlist *, cpl_frameset *);

static char fors_config_description[] =
"This recipe is used to create the so-called GRISM_TABLE, containing all\n"
"the FORS spectral pipeline configuration parameters related to a specific\n"
"grism. This is a way to provide for each specific instrument mode a set of\n"
"appropriate defaults for the recipe parameters.\n"
"The values assigned to each input parameter of fors_config are simply\n"
"copied to a FITS table consisting of one row, and as many columns as the\n"
"input parameter: each column will have the same name and type of each\n"
"parameter. Only the three parameters \"instrument\", \"grism\", and\n"
"\"id\" are not written to the table columns, but to the descriptor header\n"
"keywords INSTRUME, ESO INS GRIS1 NAME, and ESO INS GRIS1 ID, that will be\n"
"used by the automatic pipeline for appropriate data association.\n\n"
"Input files: none\n\n"
"  DO category:               Type:       Explanation:         Required:\n"
"Output files:\n\n"
"  DO category:               Data type:  Explanation:\n"
"  GRISM_TABLE                FITS table  Recipe configuration parameters\n\n";

#define fors_config_exit(message)             \
{                                             \
if ((const char *)message != NULL) cpl_msg_error(recipe, message);  \
cpl_table_delete(table);                      \
cpl_propertylist_delete(header);              \
cpl_free(filename);                           \
return -1;                                    \
}


/**
 * @brief    Build the list of available plugins, for this module. 
 *
 * @param    list    The plugin list
 *
 * @return   0 if everything is ok, -1 otherwise
 *
 * Create the recipe instance and make it available to the application 
 * using the interface. This function is exported.
 */

int cpl_plugin_get_info(cpl_pluginlist *list)
{
    cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe );
    cpl_plugin *plugin = &recipe->interface;

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    FORS_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "fors_config",
                    "Creation of FORS recipes configuration tables",
                    fors_config_description,
                    "Carlo Izzo",
                    PACKAGE_BUGREPORT,
    "This file is currently part of the FORS Instrument Pipeline\n"
    "Copyright (C) 2002-2010 European Southern Observatory\n\n"
    "This program is free software; you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation; either version 2 of the License, or\n"
    "(at your option) any later version.\n\n"
    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n"
    "GNU General Public License for more details.\n\n"
    "You should have received a copy of the GNU General Public License\n"
    "along with this program; if not, write to the Free Software Foundation,\n"
    "Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA\n",
                    fors_config_create,
                    fors_config_exec,
                    fors_config_destroy);

    cpl_pluginlist_append(list, plugin);
    
    return 0;
}


/**
 * @brief    Setup the recipe options    
 *
 * @param    plugin  The plugin
 *
 * @return   0 if everything is ok
 *
 * Defining the command-line/configuration parameters for the recipe.
 */

static int fors_config_create(cpl_plugin *plugin)
{
    cpl_recipe    *recipe;
    cpl_parameter *p;


    /* 
     * Check that the plugin is part of a valid recipe 
     */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin;
    else 
        return -1;

    /* 
     * Create the parameters list in the cpl_recipe object 
     */

    recipe->parameters = cpl_parameterlist_new(); 


    /*
     * Dispersion
     */

    p = cpl_parameter_new_value("fors.fors_config.dispersion",
                                CPL_TYPE_DOUBLE,
                                "Expected spectral dispersion (Angstrom/pixel)",
                                "fors.fors_config",
                                0.0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "dispersion");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Peak detection level
     */

    p = cpl_parameter_new_value("fors.fors_config.peakdetection",
                                CPL_TYPE_DOUBLE,
                                "Peak detection threshold (ADU)",
                                "fors.fors_config",
                                250.0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "peakdetection");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /* 
     * Degree of wavelength calibration polynomial
     */

    p = cpl_parameter_new_value("fors.fors_config.wdegree",
                                CPL_TYPE_INT,
                                "Degree of wavelength calibration polynomial",
                                "fors.fors_config",
                                4);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "wdegree");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Reference lines search radius
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.wradius",
                                CPL_TYPE_INT,
                                "Search radius if iterating pattern-matching "
                                "with first-guess method",
                                "fors.fors_config",
                                0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "wradius");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Rejection threshold in dispersion relation polynomial fitting
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.wreject",
                                CPL_TYPE_DOUBLE,
                                "Rejection threshold in dispersion "
                                "relation fit (pixel)",
                                "fors.fors_config",
                                0.7);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "wreject");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Wavelength solution interpolation (for LSS data)
     */
 
/*
    p = cpl_parameter_new_value("fors.fors_config.wmode",
                                CPL_TYPE_INT,
                                "Interpolation mode of wavelength solution "
                                "applicable to LSS-like data (0 = no "
                                "interpolation, 1 = fill gaps, 2 = global "
                                "model",
                                "fors.fors_config",
                                0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "wmode");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Degree of spectral curvature polynomial
     */

    p = cpl_parameter_new_value("fors.fors_config.cdegree",
                                CPL_TYPE_INT,
                                "Degree of spectral curvature polynomial",
                                "fors.fors_config",
                                4);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "cdegree");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Global curvature model
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.cglobal",
                                CPL_TYPE_BOOL,
                                "Global curvature model",
                                "fors.fors_config",
                                TRUE);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "cglobal");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Start wavelength for spectral extraction
     */

    p = cpl_parameter_new_value("fors.fors_config.startwavelength",
                                CPL_TYPE_DOUBLE,
                                "Start wavelength in spectral extraction",
                                "fors.fors_config",
                                0.0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "startwavelength");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * End wavelength for spectral extraction
     */

    p = cpl_parameter_new_value("fors.fors_config.endwavelength",
                                CPL_TYPE_DOUBLE,
                                "End wavelength in spectral extraction",
                                "fors.fors_config",
                                0.0);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "endwavelength");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Try slit identification
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.slit_ident",
                                CPL_TYPE_BOOL,
                                "Attempt slit identification for MOS or MXU",
                                "fors.fors_config",
                                TRUE);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "slit_ident");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Degree of flat field fitting polynomial along spatial direction 
     * (used for LSS data)
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.sdegree",
                                CPL_TYPE_INT,
                                "Degree of flat field fitting polynomial "
                                "along spatial direction (used for LSS "
                                "data only)",
                                "fors.fors_config",
                                4);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "sdegree");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Degree of flat field fitting polynomial along dispersion direction
     * (used for MOS and MXU data)
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.ddegree",
                                CPL_TYPE_INT,
                                "Degree of flat field fitting polynomial "
                                "along dispersion direction (used for MOS "
                                "and MXU data only)",
                                "fors.fors_config",
                                7);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ddegree");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Smooth box radius for flat field along dispersion direction
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.dradius",
                                CPL_TYPE_INT,
                                "Smooth box radius for flat field along "
                                "dispersion direction",
                                "fors.fors_config",
                                10);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "dradius");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Smooth box radius for flat field along spatial direction
     * (used for LSS data only)
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.sradius",
                                CPL_TYPE_INT,
                                "Smooth box radius for flat field along "
                                "spatial direction (used for LSS data only)",
                                "fors.fors_config",
                                10);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "sradius");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /* 
     * Sky lines alignment
     */

/*
    p = cpl_parameter_new_value("fors.fors_config.skyalign",
                                CPL_TYPE_INT,
                                "Polynomial order for sky lines alignment, "
                                "or -1 to avoid alignment",
                                "fors.fors_config",
                                -1);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skyalign");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);
*/

    /*
     * Instrument name
     */

    p = cpl_parameter_new_value("fors.fors_config.instrument",
                                CPL_TYPE_STRING,
                                "Name of instrument",
                                "fors.fors_config",
                                "0");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "instrument");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Grism name
     */

    p = cpl_parameter_new_value("fors.fors_config.grism",
                                CPL_TYPE_STRING,
                                "Name of grism",
                                "fors.fors_config",
                                "0");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "grism");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Grism id
     */

    p = cpl_parameter_new_value("fors.fors_config.grism_id",
                                CPL_TYPE_STRING,
                                "Grism ID",
                                "fors.fors_config",
                                "0");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "grism_id");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Filter name
     */

    p = cpl_parameter_new_value("fors.fors_config.filter",
                                CPL_TYPE_STRING,
                                "Name of filter",
                                "fors.fors_config",
                                "0");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "filter");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    /*
     * Filter id
     */

    p = cpl_parameter_new_value("fors.fors_config.filter_id",
                                CPL_TYPE_STRING,
                                "Filter ID",
                                "fors.fors_config",
                                "0");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "filter_id");
    cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);
    cpl_parameterlist_append(recipe->parameters, p);

    return 0;
}


/**
 * @brief    Execute the plugin instance given by the interface
 *
 * @param    plugin  the plugin
 *
 * @return   0 if everything is ok
 */

static int fors_config_exec(cpl_plugin *plugin)
{
    cpl_recipe *recipe;
    
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin;
    else 
        return -1;

    return fors_config(recipe->parameters, recipe->frames);
}


/**
 * @brief    Destroy what has been created by the 'create' function
 *
 * @param    plugin  The plugin
 *
 * @return   0 if everything is ok
 */

static int fors_config_destroy(cpl_plugin *plugin)
{
    cpl_recipe *recipe;
    
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin;
    else 
        return -1;

    cpl_parameterlist_delete(recipe->parameters); 

    return 0;
}


/**
 * @brief    Interpret the command line options and execute the data processing
 *
 * @param    parlist     The parameters list
 * @param    frameset    The set-of-frames
 *
 * @return   0 if everything is ok
 */

static int fors_config(cpl_parameterlist *parlist, cpl_frameset *frameset)
{

    const char *recipe = "fors_config";


    /*
     * Input parameters
     */

    double      dispersion;
    double      peakdetection;
    int         wdegree;
    int         cdegree;
    double      startwavelength;
    double      endwavelength;
/*
 *    int         wradius;
 *    double      wreject;
 *    int         wmode;
 *    int         cglobal;
 *    int         slit_ident;
 *    int         sdegree;
 *    int         ddegree;
 *    int         sradius;
 *    int         dradius;
 *    int         skyalign;
 */

    const char *instrument;
    const char *grism;
    const char *grism_id;
    const char *filter;
    const char *filter_id;

    char       *filename = NULL;

    /*
     * CPL objects
     */

    cpl_table        *table  = NULL;
    cpl_propertylist *header = NULL;

    /*
     * Auxiliary variables
     */

    int     len;


    if (frameset){}   /* To avoid compiler warning */

    /* 
     * Get configuration parameters
     */

    cpl_msg_info(recipe, "Recipe %s configuration parameters:", recipe);
    cpl_msg_indent_more();

    table = cpl_table_new(1);

    dispersion = dfs_get_parameter_double(parlist, 
                                          "fors.fors_config.dispersion", NULL);

    if (dispersion <= 0.0)
        fors_config_exit("Invalid spectral dispersion value");

    cpl_table_new_column(table, "dispersion", CPL_TYPE_DOUBLE);
    cpl_table_set_double(table, "dispersion", 0, dispersion);

    peakdetection = dfs_get_parameter_double(parlist, 
                               "fors.fors_config.peakdetection", NULL);
    if (peakdetection <= 0.0)
        fors_config_exit("Invalid peak detection level");

    cpl_table_new_column(table, "peakdetection", CPL_TYPE_DOUBLE);
    cpl_table_set_double(table, "peakdetection", 0, peakdetection);

    wdegree = dfs_get_parameter_int(parlist, "fors.fors_config.wdegree", NULL);

    if (wdegree < 1)
        fors_config_exit("Invalid polynomial degree");

    if (wdegree > 5)
        fors_config_exit("Max allowed polynomial degree is 5");

    cpl_table_new_column(table, "wdegree", CPL_TYPE_INT);
    cpl_table_set_int(table, "wdegree", 0, wdegree);

/*
 *    wradius = dfs_get_parameter_int(parlist, 
 *                                    "fors.fors_config.wradius", NULL);
 * 
 *    if (wradius < 0)
 *        fors_config_exit("Invalid search radius");
 * 
 *    cpl_table_new_column(table, "wradius", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "wradius", 0, wradius);
 * 
 *    wreject = dfs_get_parameter_double(parlist, 
 *                                       "fors.fors_config.wreject", NULL);
 * 
 *    if (wreject <= 0.0)
 *        fors_config_exit("Invalid rejection threshold");
 * 
 *    cpl_table_new_column(table, "wreject", CPL_TYPE_DOUBLE);
 *    cpl_table_set_double(table, "wreject", 0, wreject);
 * 
 *    wmode = dfs_get_parameter_int(parlist, "fors.fors_config.wmode", NULL);
 * 
 *    if (wmode < 0 || wmode > 2)
 *        fors_config_exit("Invalid wavelength solution interpolation mode");
 * 
 *    cpl_table_new_column(table, "wmode", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "wmode", 0, wmode);
 */

    cdegree = dfs_get_parameter_int(parlist, "fors.fors_config.cdegree", NULL);

    if (cdegree < 1)
        fors_config_exit("Invalid polynomial degree");

    if (cdegree > 5)
        fors_config_exit("Max allowed polynomial degree is 5");

    cpl_table_new_column(table, "cdegree", CPL_TYPE_INT);
    cpl_table_set_int(table, "cdegree", 0, cdegree);

/*
 *    cglobal = dfs_get_parameter_bool(parlist, "fors.fors_config.cglobal", 
 *                                     NULL);
 *
 *    cpl_table_new_column(table, "cglobal", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "cglobal", 0, cglobal);
 */

    startwavelength = dfs_get_parameter_double(parlist, 
                                 "fors.fors_config.startwavelength", NULL);
    if (startwavelength > 1.0)
        if (startwavelength < 3000.0 || startwavelength > 13000.0)
            fors_config_exit("Invalid wavelength");

    cpl_table_new_column(table, "startwavelength", CPL_TYPE_DOUBLE);
    cpl_table_set_double(table, "startwavelength", 0, startwavelength);

    endwavelength = dfs_get_parameter_double(parlist, 
                                   "fors.fors_config.endwavelength", NULL);
    if (endwavelength > 1.0)
        if (endwavelength < 3000.0 || endwavelength > 13000.0)
            fors_config_exit("Invalid wavelength");

    if (startwavelength > 1.0 && endwavelength > 1.0)
        if (endwavelength - startwavelength <= 0.0)
            fors_config_exit("Invalid wavelength interval");

    cpl_table_new_column(table, "endwavelength", CPL_TYPE_DOUBLE);
    cpl_table_set_double(table, "endwavelength", 0, endwavelength);

/*
 *    slit_ident = dfs_get_parameter_bool(parlist, 
 *                                   "fors.fors_config.slit_ident", NULL);
 *
 *    cpl_table_new_column(table, "slit_ident", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "slit_ident", 0, slit_ident);
 *
 *    sdegree = dfs_get_parameter_int(parlist, 
 *                                    "fors.fors_config.sdegree", NULL);
 *
 *    cpl_table_new_column(table, "sdegree", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "sdegree", 0, sdegree);
 *
 *    ddegree = dfs_get_parameter_int(parlist, 
 *                                    "fors.fors_config.ddegree", NULL);
 *
 *    cpl_table_new_column(table, "ddegree", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "ddegree", 0, ddegree);
 *
 *    sradius = dfs_get_parameter_int(parlist, 
 *                                    "fors.fors_config.sradius", NULL);
 *    dradius = dfs_get_parameter_int(parlist, 
 *                                    "fors.fors_config.dradius", NULL);
 *
 *    if (sradius < 1 || dradius < 1)
 *        fors_config_exit("Invalid smoothing box radius");
 *
 *    cpl_table_new_column(table, "sradius", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "sradius", 0, sradius);
 *    cpl_table_new_column(table, "dradius", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "dradius", 0, dradius);
 *
 *    skyalign = dfs_get_parameter_int(parlist, "fors.fors_config.skyalign", 
 *                                     NULL);
 *
 *    cpl_table_new_column(table, "skyalign", CPL_TYPE_INT);
 *    cpl_table_set_int(table, "skyalign", 0, skyalign);
 */

    header = cpl_propertylist_new();

    instrument = dfs_get_parameter_string(parlist, 
                                   "fors.fors_config.instrument", NULL);
    cpl_propertylist_update_string(header, "INSTRUME", instrument);

    grism = dfs_get_parameter_string(parlist, "fors.fors_config.grism", NULL);
    cpl_propertylist_update_string(header, "ESO INS GRIS1 NAME", grism);

    grism_id = dfs_get_parameter_string(parlist, 
                                   "fors.fors_config.grism_id", NULL);
    cpl_propertylist_update_string(header, "ESO INS GRIS1 ID", grism_id);

    filter = dfs_get_parameter_string(parlist, "fors.fors_config.filter", NULL);
    cpl_propertylist_update_string(header, "ESO INS FILT1 NAME", filter);

    filter_id = dfs_get_parameter_string(parlist, 
                                   "fors.fors_config.filter_id", NULL);
    cpl_propertylist_update_string(header, "ESO INS FILT1 ID", filter_id);

    if (cpl_error_get_code())
        fors_config_exit("Failed to get the configuration parameters");

    cpl_propertylist_update_string(header, "ESO PRO CATG", "GRISM_TABLE");

    len = 14;
    len += strlen(instrument);
    len += strlen(grism + 5);
    len += strlen(grism_id);
    len += strlen(filter);
    len += strlen(filter_id);

    filename = cpl_calloc(len, sizeof(char));

    sprintf(filename, "%s_GRS_%s_%s_%s_%s.fits", 
            instrument, grism + 5, grism_id+1, filter, filter_id+1);

    cpl_table_save(table, header, NULL, filename, CPL_IO_DEFAULT);
    cpl_propertylist_delete(header); header = NULL;
    cpl_table_delete(table); table = NULL;
    cpl_free(filename); filename = NULL;

    if (cpl_error_get_code()) {
        cpl_msg_error(cpl_error_get_where(), "%s", cpl_error_get_message());
        fors_config_exit(NULL);
    }
    else 
        return 0;
}
