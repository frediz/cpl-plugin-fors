## Process this file with automake to produce Makefile.in

##   This file is part of the FORS Pipeline
##   Copyright (C) 2001-2006 European Southern Observatory

##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Library General Public
##   License as published by the Free Software Foundation; either
##   version 2 of the License, or (at your option) any later version.

##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Library General Public License for more details.

##   You should have received a copy of the GNU Library General Public License
##   along with this library; see the file COPYING.LIB.  If not, write to
##   the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
##   Boston, MA 02111-1307, USA.

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~

SUBDIRS = . tests

if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif


AM_CPPFLAGS = $(HDRL_INCLUDES) $(MOSCA_INCLUDES) $(GSL_CFLAGS) $(all_includes) 


noinst_HEADERS = 

plugin_LTLIBRARIES = fors_calib.la fors_science.la fors_config.la \
                     fors_sumflux.la fors_extract.la \
                     fors_bias.la \
                     fors_remove_bias.la fors_spec_mflat.la \
                     fors_detect_spectra.la \
                     fors_trace_flat.la fors_normalise_flat.la \
                     fors_flatfield.la fors_extract_slits.la \
                     fors_wave_calib.la fors_wave_calib_lss.la \
                     fors_align_sky.la fors_align_sky_lss.la \
                     fors_subtract_sky.la fors_resample.la \
                     fors_detect_objects.la fors_extract_objects.la \
                     fors_subtract_sky_lss.la fors_dark.la \
                     fors_img_screen_flat.la fors_img_sky_flat.la \
                     fors_zeropoint.la fors_photometry.la fors_img_science.la \
                     fors_pmos_calib.la fors_pmos_science.la \
                     fors_pmos_extract.la

fors_calib_la_SOURCES = fors_calib.cc
fors_calib_la_LIBADD = $(HDRL_LIBS) $(LIBMOSCA) $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_calib_la_LDFLAGS = $(HDRL_LDFLAGS) $(CPL_LDFLAGS) -module -avoid-version
fors_calib_la_DEPENDENCIES = $(LIBFORS)

fors_science_la_SOURCES = fors_science.cc
fors_science_la_LIBADD = $(HDRL_LIBS) $(LIBMOSCA) $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_science_la_LDFLAGS = $(HDRL_LDFLAGS) $(CPL_LDFLAGS) -module -avoid-version 
fors_science_la_DEPENDENCIES = $(LIBFORS)

fors_config_la_SOURCES = fors_config.c
fors_config_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_config_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_config_la_DEPENDENCIES = $(LIBFORS)

fors_sumflux_la_SOURCES = fors_sumflux.c
fors_sumflux_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_sumflux_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_sumflux_la_DEPENDENCIES = $(LIBFORS)

fors_extract_la_SOURCES = fors_extract.c
fors_extract_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_extract_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_extract_la_DEPENDENCIES = $(LIBFORS)

fors_bias_la_SOURCES = fors_bias.c
fors_bias_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_bias_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_bias_la_DEPENDENCIES = $(LIBFORS)

fors_remove_bias_la_SOURCES = fors_remove_bias.cc
fors_remove_bias_la_LIBADD = $(HDRL_LIBS) $(LIBMOSCA) $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_remove_bias_la_LDFLAGS = $(HDRL_LDFLAGS) $(CPL_LDFLAGS) -module -avoid-version 
fors_remove_bias_la_DEPENDENCIES = $(LIBFORS)

fors_spec_mflat_la_SOURCES = fors_spec_mflat.cc
fors_spec_mflat_la_LIBADD = $(HDRL_LIBS) $(LIBMOSCA) $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_spec_mflat_la_LDFLAGS = $(HDRL_LDFLAGS) $(CPL_LDFLAGS) -module -avoid-version 
fors_spec_mflat_la_DEPENDENCIES = $(LIBFORS)

fors_detect_spectra_la_SOURCES = fors_detect_spectra.c
fors_detect_spectra_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_detect_spectra_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_detect_spectra_la_DEPENDENCIES = $(LIBFORS)

fors_trace_flat_la_SOURCES = fors_trace_flat.c
fors_trace_flat_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_trace_flat_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_trace_flat_la_DEPENDENCIES = $(LIBFORS)

fors_normalise_flat_la_SOURCES = fors_normalise_flat.cc
fors_normalise_flat_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_normalise_flat_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_normalise_flat_la_DEPENDENCIES = $(LIBFORS)

fors_flatfield_la_SOURCES = fors_flatfield.c
fors_flatfield_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_flatfield_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_flatfield_la_DEPENDENCIES = $(LIBFORS)

fors_extract_slits_la_SOURCES = fors_extract_slits.c
fors_extract_slits_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_extract_slits_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_extract_slits_la_DEPENDENCIES = $(LIBFORS)

fors_wave_calib_la_SOURCES = fors_wave_calib.c
fors_wave_calib_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_wave_calib_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_wave_calib_la_DEPENDENCIES = $(LIBFORS)

fors_wave_calib_lss_la_SOURCES = fors_wave_calib_lss.c
fors_wave_calib_lss_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_wave_calib_lss_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_wave_calib_lss_la_DEPENDENCIES = $(LIBFORS)

fors_align_sky_la_SOURCES = fors_align_sky.c
fors_align_sky_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_align_sky_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_align_sky_la_DEPENDENCIES = $(LIBFORS)

fors_align_sky_lss_la_SOURCES = fors_align_sky_lss.c
fors_align_sky_lss_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_align_sky_lss_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_align_sky_lss_la_DEPENDENCIES = $(LIBFORS)

fors_subtract_sky_la_SOURCES = fors_subtract_sky.c
fors_subtract_sky_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_subtract_sky_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_subtract_sky_la_DEPENDENCIES = $(LIBFORS)

fors_subtract_sky_lss_la_SOURCES = fors_subtract_sky_lss.c
fors_subtract_sky_lss_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_subtract_sky_lss_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_subtract_sky_lss_la_DEPENDENCIES = $(LIBFORS)

fors_resample_la_SOURCES = fors_resample.c
fors_resample_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_resample_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_resample_la_DEPENDENCIES = $(LIBFORS)

fors_detect_objects_la_SOURCES = fors_detect_objects.c
fors_detect_objects_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_detect_objects_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_detect_objects_la_DEPENDENCIES = $(LIBFORS)

fors_extract_objects_la_SOURCES = fors_extract_objects.c
fors_extract_objects_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_extract_objects_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_extract_objects_la_DEPENDENCIES = $(LIBFORS)

fors_dark_la_SOURCES = fors_dark.c
fors_dark_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_dark_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_dark_la_DEPENDENCIES = $(LIBFORS)

fors_img_screen_flat_la_SOURCES = fors_img_screen_flat.c
fors_img_screen_flat_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_img_screen_flat_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_img_screen_flat_la_DEPENDENCIES = $(LIBFORS)

fors_img_sky_flat_la_SOURCES = fors_img_sky_flat.c
fors_img_sky_flat_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_img_sky_flat_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_img_sky_flat_la_DEPENDENCIES = $(LIBFORS)

fors_zeropoint_la_SOURCES = fors_zeropoint.c
fors_zeropoint_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_zeropoint_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_zeropoint_la_DEPENDENCIES = $(LIBFORS)

fors_photometry_la_SOURCES = fors_photometry.c
fors_photometry_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_photometry_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_photometry_la_DEPENDENCIES = $(LIBFORS)

fors_img_science_la_SOURCES = fors_img_science.c
fors_img_science_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_img_science_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_img_science_la_DEPENDENCIES = $(LIBFORS)

fors_pmos_calib_la_SOURCES = fors_pmos_calib.c
fors_pmos_calib_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_pmos_calib_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_pmos_calib_la_DEPENDENCIES = $(LIBFORS)

fors_pmos_science_la_SOURCES = fors_pmos_science.c
fors_pmos_science_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_pmos_science_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_pmos_science_la_DEPENDENCIES = $(LIBFORS)

fors_pmos_extract_la_SOURCES = fors_pmos_extract.c
fors_pmos_extract_la_LIBADD = $(LIBFORS) $(LIBCPLCORE) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLUI)
fors_pmos_extract_la_LDFLAGS = $(CPL_LDFLAGS) -module -avoid-version 
fors_pmos_extract_la_DEPENDENCIES = $(LIBFORS)
