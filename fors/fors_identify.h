/* $Id: fors_identify.h,v 1.10 2013-09-10 15:16:32 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-09-10 15:16:32 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_IDENTIFY_H
#define FORS_IDENTIFY_H

#include <fors_star.h>

#include <cpl.h>

CPL_BEGIN_DECLS

typedef struct _identify_method identify_method;

void 
fors_identify_define_parameters(cpl_parameterlist *parameters, 
				const char *context);
identify_method *
fors_identify_method_new(const cpl_parameterlist *parameters, const char *context);

void
fors_identify_method_delete(identify_method **im);

void
fors_identify(fors_star_list *stars, 
              fors_std_star_list *cat,
              const identify_method *im,
              cpl_image **histogram);


CPL_END_DECLS

#endif
