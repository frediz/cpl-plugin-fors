/* $Id: fors_double.h,v 1.4 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_DOUBLE_H
#define FORS_DOUBLE_H

#undef LIST_DEFINE
#undef LIST_ELEM
#define LIST_ELEM double
#include <list.h>

double *
double_duplicate(const double *d);

void
double_delete(double **d);

double
double_eval(const double *d, void *data);

double
double_divide(double x, double dx,
	      double y, double dy,
	      double *error);

double
double_subtract(double x, double dx,
                double y, double dy,
                double *error);

double
double_atan2(double y, double dy,
             double x, double dx,
	     double *error);

#endif
