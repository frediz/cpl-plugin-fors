/* $Id: fors_stack.h,v 1.7 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_STACK_IMPL_H
#define FORS_STACK_IMPL_H

#include <fors_image.h>
#include <cpl.h>

CPL_BEGIN_DECLS

typedef struct _stack_method
{
    enum {AVERAGE, MEAN, WMEAN, MEDIAN, MINMAX, KSIGMA} method;
    const char *method_name;
    union {
        struct {
            int min_reject;
            int max_reject;
        } minmax;
        struct {
            double klow;
            double khigh;
            int kiter;
        } ksigma;
    } pars;
} stack_method;

void 
fors_stack_define_parameters(cpl_parameterlist *parameters, 
			     const char *context, const char *default_method);
stack_method *
fors_stack_method_new(const cpl_parameterlist *parameters, const char *context);

const char *fors_stack_method_get_string(const stack_method *sm);

void
fors_stack_method_delete(stack_method **sm);

fors_image *
fors_stack(fors_image_list *images, const stack_method *sm);

fors_image *
fors_stack_const(const fors_image_list *images, const stack_method *sm);

CPL_END_DECLS

#endif
