/* $Id: fors_std_cat.h,v 1.13 2013-09-10 15:19:58 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-09-10 15:19:58 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_STD_CAT_H
#define FORS_STD_CAT_H

#include <fors_std_star.h>
#include <cpl.h>

extern const char *FORS_STD_CAT_COLUMN_RA;
extern const char *FORS_STD_CAT_COLUMN_DEC;
extern const char *FORS_STD_CAT_COLUMN_NAME;

CPL_BEGIN_DECLS

fors_std_star_list *
fors_std_cat_load(                          const cpl_frameset  *cat_frames,
                                            char            band,
                                            bool            require_all_frames,
                                            double          color_term,
                                            double          dcolor_term);

fors_std_star_list *
fors_std_cat_load_old(                      const cpl_frameset *cat_frames,
                                            /*const fors_setting *setting,*/
                                            char  optical_band,
                                            double color_term,
                                            double dcolor_term);

CPL_END_DECLS
#endif  /* FORS_STD_CAT_H */
