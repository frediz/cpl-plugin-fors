/*
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FORS_CCD_CONFIG_H
#define FORS_CCD_CONFIG_H

#include <memory>
#include "fiera_config.h"

namespace fors 
{

/*
 * This class represents the CCD configuration of the FIERA controlled detectors
 * It inherits from mosca::fiera_config but it modifies the pre/overscan regions
 * in some cases due to bad performance of some detectors.
 * Ticket PIPE-6125 contains the background about this. 
 */
class fiera_config : public mosca::fiera_config
{
public:
    
    fiera_config(const cpl_propertylist * header);
    
    fiera_config();

    virtual ~fiera_config();
};

std::auto_ptr<fors::fiera_config> ccd_settings_equal(const cpl_frameset * fset);

void update_ccd_ron(mosca::ccd_config ccd_config, 
                         cpl_propertylist * master_bias_header);

std::auto_ptr<fors::fiera_config>  ccd_config_read
(const cpl_frame * target, const cpl_frame * bias_frame);


} /* namespace fors */

#endif /* FORS_CCD_CONFIG_H */

