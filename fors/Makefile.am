## Process this file with automake to produce Makefile.in

##   This file is part of the FORS Pipeline
##   Copyright (C) 2000-2004 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~

SUBDIRS = . tests


if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif


AM_CPPFLAGS = $(HDRL_INCLUDES) $(MOSCA_INCLUDES) $(CPL_INCLUDES) $(GSL_CFLAGS)

AM_CXXFLAGS = -Wall -Wextra 

pkginclude_HEADERS = fors_bias_impl.h \
                     fors_bpm.h \
                     fors_calibrated_slits.h \
                     fors_ccd_config.h \
                     fors_dark_impl.h \
                     fors_data.h \
                     fors_detected_slits.h \
                     fors_detmodel.h \
                     fors_dfs.h \
                     fors_double.h \
                     fors_extract.h \
                     fors_flat_normalise.h \
                     fors_grism.h \
                     fors_header.h \
                     fors_identify.h \
                     fors_image.h \
                     fors_img_science_impl.h \
                     fors_img_screen_flat_impl.h \
                     fors_img_sky_flat_impl.h \
                     fors_instrument.h \
                     fors_overscan.h \
                     fors_paf.h \
                     fors_pattern.h \
                     fors_pfits.h \
                     fors_photometry_impl.h \
                     fors_point.h \
                     fors_polynomial.h \
                     fors_preprocess.h \
                     fors_qc.h \
                     fors_stats.h \
                     fors_recipe_impl.h \
                     fors_response.h \
                     fors_saturation.h \
                     fors_saturation_mos.h \
                     fors_subtract_bias.h \
                     fors_setting.h \
                     fors_stack.h \
                     fors_star.h \
                     fors_std_cat.h \
                     fors_std_star.h \
                     fors_tools.h \
                     fors_utils.h \
                     fors_zeropoint_impl.h \
                     list.h \
                     list_void.h \
		     fors_dfs_idp.h \
                     moses.h

#lib_LTLIBRARIES = libfors.la
privatelib_LTLIBRARIES = libfors.la

libfors_la_SOURCES = fors_bias_impl.cc \
                     fors_bpm.cc \
                     fors_calibrated_slits.cc \
                     fors_ccd_config.cc \
                     fors_dark_impl.cc \
                     fors_data.c \
                     fors_detected_slits.cc \
                     fors_detmodel.cc \
                     fors_dfs.c \
                     fors_double.c \
                     fors_extract.c \
                     fors_flat_normalise.cc \
                     fors_grism.cc \
                     fors_header.c \
                     fors_identify.c \
                     fors_image.c \
                     fors_img_science_impl.cc \
                     fors_img_screen_flat_impl.cc \
                     fors_img_sky_flat_impl.cc \
                     fors_instrument.c \
                     fors_overscan.cc \
                     fors_paf.c \
                     fors_pattern.c \
                     fors_pfits.c \
                     fors_photometry_impl.cc \
                     fors_point.c \
                     fors_polynomial.c \
                     fors_preprocess.cc \
                     fors_qc.c \
                     fors_stats.c \
                     fors_response.cc \
                     fors_saturation.cc \
                     fors_saturation_mos.cc \
                     fors_subtract_bias.c \
                     fors_setting.c \
                     fors_stack.c \
                     fors_star.c \
                     fors_std_cat.c \
                     fors_std_star.c \
                     fors_tools.c \
                     fors_utils.c \
                     fors_zeropoint_impl.cc \
                     list.c \
		     fors_dfs_idp.c \
                     moses.c

libfors_la_LDFLAGS = $(HDRL_LDFLAGS) $(CPL_LDFLAGS) -version-info $(LT_CURRENT):$(LT_REVISION):$(LT_AGE) -no-undefined
libfors_la_LIBADD = $(HDRL_LIBS) $(LIBMOSCA) $(LIBIRPLIB) $(LIBCPLUI) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLCORE)
libfors_la_DEPENDENCIES = $(LIBIRPLIB) $(LIBMOSCA)

# distributed config files
dist_config_DATA = fors.conv fors.nnw fors.param

# non-distributed config files 
config_DATA = fors.sex fors_landolt.sex
#TODO: Make the substitution in configure.ac
fors_landolt.sex: $(srcdir)/fors_landolt.sex.tpl
	cat $(srcdir)/fors_landolt.sex.tpl | sed "s|FORS_SEXTRACTOR_CONFIG|@configdir@|" > fors_landolt.sex
fors.sex: $(srcdir)/fors.sex.tpl
	cat $(srcdir)/fors.sex.tpl | sed "s|FORS_SEXTRACTOR_CONFIG|@configdir@|" > fors.sex

EXTRA_DIST = fors.sex.tpl fors_landolt.sex.tpl

#noinst_PROGRAMS = create_stetson \
#                  create_phot

#create_stetson_LDFLAGS = $(CPL_LDFLAGS)
#create_stetson_LDADD = $(LIBFORS) $(LIBMOSCA) $(GSL_LIBS) $(LIBCPLUI) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLCORE)
#create_stetson_DEPENDENCIES = libfors.la

#create_phot_LDFLAGS = $(CPL_LDFLAGS)
#create_phot_LDADD = $(LIBFORS) $(LIBMOSCA) $(GSL_LIBS) $(LIBCPLUI) $(LIBCPLDFS) $(LIBCPLDRS) $(LIBCPLCORE)
#create_phot_DEPENDENCIES = libfors.la

clean-local:
	$(RM) fors.sex fors_landolt.sex
