/* $Id: list_void.h,v 1.10 2013-05-16 08:40:07 cgarcia Exp $
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-05-16 08:40:07 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */

#ifndef LIST_VOID_H
#define LIST_VOID_H

#include <stdbool.h>
#include <stdio.h>

typedef struct list list;
typedef double (*list_func_eval)(const void *element,
				 void *data);

typedef bool (*list_func_lt)(const void *e1,
			     const void *e2,
			     void *data);

typedef bool (*list_func_predicate)(const void *, void *);


/* Create */
list *list_new(void);

list *list_duplicate(const list *l, void *(*duplicate)(const void *));

/* Delete */
void
list_delete(list **l, void (*ldelete)(void **));

void
list_delete_const(const list **l, void (*ldelete)(void **));

int
list_size(const list *l);

/* Iterate */
void *
list_first(list *l);

void *
list_next(list *l);

const void *
list_first_const(const list *l);

const void *
list_next_const(const list *l);

void
list_first_pair(list *l,
		void **e1,
		void **e2);

void
list_next_pair(list *l,
	       void **e1,
	       void **e2);

void
list_first_pair_const(const list *l,
		      const void **e1,
		      const void **e2);

void
list_next_pair_const(const list *l,
		     const void **e1,
		     const void **e2);


/* Element */
void
list_insert(list *l, void *e);

const void *
list_remove_const(list *l, const void *e);

void *
list_remove(list *l, void *e);

/* List */
void
list_reverse(list *l);

list *
list_extract(const list *l,
             void *(*duplicate)(const void *),
	     list_func_predicate predicate,
             void *data);

/* Search */
void *
list_min(list *l, list_func_lt less_than, void *data);

void *
list_min_val(list *l, list_func_eval eval, void *data);

void *
list_max(list *l, list_func_lt less_than, void *data);

const void *
list_max_const(const list *l, list_func_lt less_than, void *data);

void *
list_max_val(list *l, list_func_eval eval, void *data);

void *
list_kth(list *l, int k, list_func_lt less_than, void *data);

const void *
list_kth_const(const list *l, int k, list_func_lt less_than, void *data);

void *
list_kth_val(list *l, int k, list_func_eval eval, void *data);

const void *
list_kth_val_const(const list *l, int k, list_func_eval eval, void *data);

/* Statistics */
double
list_mean(const list *l, list_func_eval eval, void *data);

double
list_mean_optimal(const list *l, 
		  list_func_eval eval, void *data_eval,
		  list_func_eval eval_err, void *data_err,
		  double *err,
                  double *red_chisq);

double
list_median(const list *l, list_func_eval eval, void *data);

double
list_mad(list *l, list_func_eval eval, void *data);

#endif
