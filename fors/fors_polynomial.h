/* $Id: fors_polynomial.h,v 1.5 2012-01-27 18:51:59 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2012-01-27 18:51:59 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_POLYNOMIAL_H
#define FORS_POLYNOMIAL_H

#include <cpl.h>

CPL_BEGIN_DECLS

int
fors_polynomial_count_coeff(                const cpl_polynomial    *p);

int
fors_polynomial_powers_find_first_coeff(    const cpl_polynomial    *p,
                                            cpl_size                *powers);

int
fors_polynomial_powers_find_next_coeff(     const cpl_polynomial    *p,
                                            cpl_size                *powers);

cpl_error_code
fors_polynomial_set_existing_coeff(         cpl_polynomial          *p,
                                            const double            *coeffs,
                                            int                     n_coeffs);

cpl_polynomial*
fors_polynomial_create_variance_polynomial( const cpl_polynomial    *p_def,
                                            const cpl_matrix        *cov_coeff);

cpl_error_code
fors_polynomial_dump(                       const cpl_polynomial    *p,
                                            const char              *name,
                                            cpl_msg_severity        level,
                                            const cpl_polynomial    *p_def);

char*
fors_polynomial_sprint_coeff(               const cpl_polynomial    *p,
                                            cpl_size                *powers,
                                            const char              *prefix);

CPL_END_DECLS

#endif  /* FORS_POLYNOMIAL_H */
