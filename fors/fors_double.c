/* $Id: fors_double.c,v 1.5 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_double.h>

#include <fors_utils.h>

#include <cpl.h>
#include <math.h>
#include <assert.h>

#define LIST_DEFINE
#undef LIST_ELEM
#define LIST_ELEM double
#include <list.h>


/**
 * @defgroup fors_double floating point values
 *
 * This module exists in order to allow operations on 
 * a container of doubles
 */

/*
  Copy constructor
 */
double *
double_duplicate(const double *d)
{
    double *dp = cpl_malloc(sizeof(*dp));
    *dp = *d;
    return dp;
}

/*
  @brief  Destructor
  @param  d    double
 */
void
double_delete(double **d)
{
    if (d && *d) {
        cpl_free(*d); *d = NULL;
    }
}

/**
 * @brief  Evaluate value
 * @param  d        to evaluate
 * @param  data     not used
 * @return value of d
 */
double
double_eval(const double *d, void *data)
{
    data = data;
    return *d;
}

#undef cleanup
#define cleanup
/**
 * @brief  Subtract numbers with errors
 * @param  x          number
 * @param  dx         error
 * @param  y          number
 * @param  dy         error
 * @param  error      (output) error of x - y assuming uncorrelated noise
 * @return x - y
 */
double
double_subtract(double x, double dx,
                double y, double dy,
                double *error)
{
    assure( error != NULL, return 0, NULL );
    
    assure( dx >= 0, return 0, NULL );
    assure( dy >= 0, return 0, NULL );

    *error = sqrt( dx*dx + dy*dy );

    return x - y;
}


#undef cleanup
#define cleanup
/**
 * @brief  Divide numbers with errors
 * @param  x          dividend
 * @param  dx         error
 * @param  y          divisor
 * @param  dy         error
 * @param  error      (output) error x/y assuming
 *                    uncorrelated noise
 * @return x/y
 */
double
double_divide(double x, double dx,
	      double y, double dy,
	      double *error)
{
    assure( error != NULL, return 0, NULL );

    assure( y*y > 0, return 0, NULL );
    assure( dx >= 0, return 0, NULL );
    assure( dy >= 0, return 0, NULL );

    *error = ( dx*dx + dy*dy * x*x / (y*y) ) / (y*y);
    *error = sqrt(*error);

    return x/y;
}


#undef cleanup
#define cleanup
/**
 * @brief  atan2 with errors
 * @param  y          first argument
 * @param  dy         error
 * @param  x          second argument
 * @param  dx         error
 * @param  error      (output) error of atan2(y,x) assuming
 *                    uncorrelated noise
 * @return atan2(y,x)
 */

double
double_atan2(double y, double dy,
             double x, double dx,
	     double *error)
{
    assure( error != NULL, return 0, NULL );
    assure( dy >= 0, return 0, NULL );
    assure( dx >= 0, return 0, NULL );
    assure( (x*x + y*y)*(x*x + y*y) > 0, return 0, NULL ); /* angle undefined */
    
    /* using error propagation formula and d(atan(u))/du = 1/(1+u^2) */
    *error = (dy*dy*x*x + dx*dx*y*y) / ((x*x + y*y)*(x*x + y*y));
    *error = sqrt(*error);

    assert( *error >= 0 );

    return atan2(y, x);
}
/**@}*/
