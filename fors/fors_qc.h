/* $Id: fors_qc.h,v 1.6 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_QC_H
#define FORS_QC_H

#include <cpl.h>

CPL_BEGIN_DECLS

extern const char *const fors_qc_dic_version;

cpl_error_code fors_qc_start_group(cpl_propertylist *, 
				   const char *, const char *);
cpl_error_code fors_qc_end_group(void);

void fors_qc_write_group_heading(const cpl_frame *raw_frame,
                                 const char *pro_catg,
                                 const char *instrument);

cpl_error_code fors_qc_write_string(const char *, const char *,
				    const char *, const char *);
cpl_error_code fors_qc_write_string_chat(const char *, const char *,
				    const char *, const char *);
cpl_error_code fors_qc_write_double(const char *, double, const char *, 
				    const char *, const char *);
cpl_error_code fors_qc_write_int(const char *, int, const char *, 
				 const char *, const char *);
cpl_error_code fors_qc_keyword_to_paf(cpl_propertylist *, const char *, 
				      const char *, const char *, const char *);
cpl_error_code fors_qc_write_qc_string(cpl_propertylist *,
				       const char *, const char *,
				       const char *, const char *);
cpl_error_code fors_qc_write_qc_int(cpl_propertylist *, int, const char *, 
				    const char *, const char *, const char *);
cpl_error_code fors_qc_write_qc_double(cpl_propertylist *, double, 
				       const char *, const char *, const char *, 
				       const char *);

CPL_END_DECLS

#endif /* FORS_QC_H */
