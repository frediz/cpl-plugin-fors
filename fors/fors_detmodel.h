/*
 * This file is part of the FORS Data Reduction Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FORS_DETMODEL_H_
#define FORS_DETMODEL_H_

#include "fors_image.h"
#include "ccd_config.h"

void fors_image_variance_from_detmodel(fors_image * image,
                                       const mosca::ccd_config& ccd_config,
                                       std::vector<double>& overscan_level);

void fors_image_variance_from_detmodel(fors_image_list * ima_list,
                                       const mosca::ccd_config& ccd_config,
                                       std::vector<double>& overscan_level);

#endif /* FORS_DETMODEL_H_ */
