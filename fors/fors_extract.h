/* $Id: fors_extract.h,v 1.11 2011-06-11 06:16:24 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2011-06-11 06:16:24 $
 * $Revision: 1.11 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_EXTRACT_H
#define FORS_EXTRACT_H

#include <fors_star.h>
#include <fors_image.h>

#include <cpl.h>

CPL_BEGIN_DECLS

typedef struct _extract_method extract_method;

typedef struct fors_extract_sky_stats {
    double mean, median, rms;
} fors_extract_sky_stats;

void 
fors_extract_define_parameters(             cpl_parameterlist *parameters, 
                                            const char *context);

bool
fors_extract_check_sex_flag(                unsigned int    sex_flag);

bool
fors_extract_check_sex_star(                const fors_star *star,
                                            const cpl_image *ref_img);


extract_method *
fors_extract_method_new(                    const cpl_parameterlist *parameters,
                                            const char *context);

void
fors_extract_method_delete(                 extract_method **em);

fors_star_list *
fors_extract(                               const fors_image *image, 
                                            const fors_setting *setting,
                                            const extract_method *em,
                                            double magsyserr,
                                            fors_extract_sky_stats *sky_stats,
                                            cpl_image **background,
                                            cpl_table **extracted_sources);


CPL_END_DECLS

#endif
