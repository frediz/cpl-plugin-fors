/* $Id: fors_pfits.c,v 1.13 2011-10-24 13:08:33 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2011-10-24 13:08:33 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_pfits.h>

/**
 * @defgroup fors_pfits  FITS keywords
 *
 * Definition of FITS header keywords
 */

/**@{*/

const char *const FORS_PFITS_INSTRUME    = "INSTRUME";
const char *const FORS_PFITS_EXPOSURE_TIME = "EXPTIME";
const char *const FORS_PFITS_AIRMASS_START = "ESO TEL AIRM START";
const char *const FORS_PFITS_AIRMASS_END = "ESO TEL AIRM END";
const char *const FORS_PFITS_FILTER_NAME = "ESO INS FILT1 NAME";
const char *const FORS_PFITS_OUTPUTS     = "ESO DET OUTPUTS";
const char *const FORS_PFITS_CONAD[4]    = {"ESO DET OUT1 CONAD",
					    "ESO DET OUT2 CONAD",
					    "ESO DET OUT3 CONAD",
					    "ESO DET OUT4 CONAD"};
const char *const FORS_PFITS_RON[4]      = {"ESO DET OUT1 RON",
					    "ESO DET OUT2 RON",
					    "ESO DET OUT3 RON",
					    "ESO DET OUT4 RON"};
const char *const FORS_PFITS_OVERSCANX   = "ESO DET OUT1 OVSCX";
const char *const FORS_PFITS_OVERSCANY   = "ESO DET OUT1 OVSCY";
const char *const FORS_PFITS_PRESCANX    = "ESO DET OUT1 PRSCX";
const char *const FORS_PFITS_PRESCANY    = "ESO DET OUT1 PRSCY";
const char *const FORS_PFITS_CRPIX1      = "CRPIX1";
const char *const FORS_PFITS_CRPIX2      = "CRPIX2";
const char *const FORS_PFITS_DET_NX      = "ESO DET OUT1 NX";
const char *const FORS_PFITS_DET_NY      = "ESO DET OUT1 NY";
const char *const FORS_PFITS_BINX        = "ESO DET WIN1 BINX";
const char *const FORS_PFITS_BINY        = "ESO DET WIN1 BINY";
const char *const FORS_PFITS_PIXSCALE    = "ESO INS PIXSCALE";
const char *const FORS_PFITS_READ_CLOCK  = "ESO DET READ CLOCK";
const char *const FORS_PFITS_CHIP_ID     = "ESO DET CHIP1 ID";
const char *const FORS_PFITS_TARG_NAME   = "ESO OBS TARG NAME";


/**@}*/
