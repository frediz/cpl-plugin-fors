/* $Id: fors_pattern-test.c,v 1.2 2007-09-07 11:29:49 jmlarsen Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-09-07 11:29:49 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_pattern.h>
#include <fors_point.h>
#include <test.h>

/**
 * @defgroup fors_pattern_test   fors_pattern unit tests
 */

/**@{*/

/**
 * @brief   test
 */
static void
test_pattern(void)
{
    fors_point *p1 = fors_point_new(1, 2);
    fors_point *p2 = fors_point_new(2, 2);
    fors_point *p3 = fors_point_new(3, 4);
    fors_point *p4 = fors_point_new(0.1, 0.2);
    fors_point *p5 = fors_point_new(0.2, 0.2);
    fors_point *p6 = fors_point_new(0.3, 0.4);
    double sigma = 0.001;

    fors_pattern *pat1 = fors_pattern_new(p1, p2, p3, sigma);
    fors_pattern *pat2 = fors_pattern_new(p4, p5, p6, sigma);

    test_abs( fors_pattern_get_scale(pat1, pat2),     10, 0.01 );
    test_abs( fors_pattern_get_scale(pat2, pat1), 1.0/10, 0.01 );
    test_abs( fors_pattern_get_angle(pat1, pat2), 0, 0.01 );
    test_abs( fors_pattern_get_angle(pat2, pat1), 0, 0.01 );
    
    test_abs( fors_pattern_distsq(pat1, pat2), 0, 0.001 );
    test_abs( fors_pattern_distsq(pat2, pat1), 0, 0.001 );

    fors_point_delete(&p1);
    fors_point_delete(&p2);
    fors_point_delete(&p3);
    fors_point_delete(&p4);
    fors_point_delete(&p5);
    fors_point_delete(&p6);
    fors_pattern_delete(&pat1);
    fors_pattern_delete(&pat2);

    return;
}


/**
 * @brief   Test of QC module
 */
int main(void)
{
    TEST_INIT;

    test_pattern();

    TEST_END;
}

/**@}*/
