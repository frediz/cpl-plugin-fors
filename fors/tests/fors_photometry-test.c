/* $Id: fors_photometry-test.c,v 1.9 2009-02-17 12:18:28 hlorch Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: hlorch $
 * $Date: 2009-02-17 12:18:28 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_photometry_impl.h>
#include <fors_dfs.h>
#include <fors_utils.h>
#include <test_simulate.h>
#include <test.h>

#include <fs_test.h>

/**
 * @defgroup fors_point_test   fors_point unit tests
 */

/**@{*/

#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    cpl_propertylist_delete(header); \
    delete_test_files(filename);\
    delete_test_file(master_fname);\
    delete_test_file(table_fname);\
    delete_test_file_from_tag(EXTINCTION_PER_NIGHT);\
    delete_test_file_from_tag(PHOT_COEFF_TABLE);\
} while(0)
/**
 * @brief   test
 */
static void
test_photometry(void)
{
    cpl_parameterlist *parameters = cpl_parameterlist_new();
    cpl_frameset *frames = cpl_frameset_new();

    const char *filename[] = {"photometry_aligned0.fits",
			      "photometry_aligned1.fits",
			      "photometry_aligned2.fits",
			      "photometry_aligned3.fits"};
    const char * master_fname = "photometry_master_flat.fits";
    const char* table_fname = "photometry_phot_table.fits";

    cpl_propertylist *header = cpl_propertylist_new();
    double exptime = 10.0;
    cpl_frameset_insert(frames, create_sky_flat(master_fname,
						MASTER_SKY_FLAT_IMG,
						CPL_FRAME_GROUP_RAW,
						10.0));

    cpl_frameset_insert(frames, create_phot_table(table_fname,
                                                  PHOT_TABLE,
                                                  CPL_FRAME_GROUP_CALIB));

    create_standard_keys(header, exptime);
    /* required by fors_photometry parameter --fite=pernight */
    cpl_propertylist_update_string(header, "ORIGIN", "ESO");
    cpl_propertylist_update_double(header, "MJD-OBS", 0.5);
    
    {
	int i;
	for (i = 0; i < (int)(sizeof(filename)/sizeof(*filename)); i++)
    {
        double  airmass;
        airmass = 1.1 + 0.1*i;
        
        /* create an airmass range for fitting the atm. ext. */
        cpl_propertylist_update_double(header, "AIRMASS", airmass);

	    /* Probably better would be to use fors_create_sources_table()
	       to create the ALIGNED_PHOT table */
	    
	    cpl_table *aligned = cpl_table_new(1);
	    
	    cpl_frame *f = cpl_frame_new();
	    cpl_frame_set_tag(f, ALIGNED_PHOT);
	    cpl_frame_set_filename(f, filename[i]);
	    cpl_frameset_insert(frames, f);
	    
	    cpl_table_new_column(aligned, "INSTR_MAG", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "DINSTR_MAG", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "MAG", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "DMAG", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "CAT_MAG", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "DCAT_MAG", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "COLOR", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "RA", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "DEC", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "X", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "Y", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "A", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "B", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "FWHM", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "THETA", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "CLASS_STAR", CPL_TYPE_DOUBLE);
	    cpl_table_new_column(aligned, "OBJECT", CPL_TYPE_STRING);
	    cpl_table_new_column(aligned, "USE_CAT", CPL_TYPE_INT);
	    
	    cpl_table_set_double(aligned, "INSTR_MAG", 0, -13.8 + (airmass-1)*0.1);
	    cpl_table_set_double(aligned, "DINSTR_MAG", 0, 0.1);
	    cpl_table_set_double(aligned, "MAG", 0, 14.4);
	    cpl_table_set_double(aligned, "DMAG", 0, 0.004);
	    cpl_table_set_double(aligned, "CAT_MAG", 0, 14.2);
	    cpl_table_set_double(aligned, "DCAT_MAG", 0, 0.002);
	    cpl_table_set_double(aligned, "COLOR", 0, 0.4);
	    
	    cpl_table_set_double(aligned, "RA", 0, 100);
	    cpl_table_set_double(aligned, "DEC", 0, 1);
	    
	    cpl_table_set_double(aligned, "X", 0, 400);
	    cpl_table_set_double(aligned, "Y", 0, 500);
	    cpl_table_set_double(aligned, "A", 0, 2);
	    cpl_table_set_double(aligned, "B", 0, 1);
	    cpl_table_set_double(aligned, "FWHM", 0, 1.4);
	    cpl_table_set_double(aligned, "THETA", 0, 0.1);
	    cpl_table_set_double(aligned, "CLASS_STAR", 0, 0.1);
	    
	    cpl_table_set_string(aligned, "OBJECT", 0, "MOBJEKT");
	    cpl_table_set_int(aligned, "USE_CAT", 0, 1);	    
	    
	    cpl_table_save(aligned, header, NULL, filename[i], CPL_IO_DEFAULT);

	    cpl_table_delete(aligned); aligned = NULL;
	}
    }

    assure( !cpl_error_get_code(), return, NULL );

    fors_photometry_define_parameters(parameters);
    cpl_parameter_set_int(cpl_parameterlist_find(parameters,
						 "fors.fors_photometry.degreef1"),
			  0);
    
    fors_photometry(frames, parameters);

    cleanup;
    return;
}


/**
 * @brief   Test of QC module
 */
int main(void)
{
    TEST_INIT;
    //cpl_msg_set_level(CPL_MSG_DEBUG);
    test_photometry();

    TEST_END;
}

/**@}*/
