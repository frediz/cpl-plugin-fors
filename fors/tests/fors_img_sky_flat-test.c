/* $Id: fors_img_sky_flat-test.c,v 1.8 2013-09-11 10:04:21 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-09-11 10:04:21 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_img_sky_flat_impl.h>
#include <fors_dfs.h>
#include <fors_utils.h>

#include <test_simulate.h>
#include <test.h>
#include <fs_test.h>

#include <cpl.h>

/**
 * @defgroup fors_img_sky_flat_test  test of sky flat recipe
 */

/**@{*/

#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    fors_image_delete(&raw_sflat); \
    fors_image_delete(&master_sflat); \
    fors_image_delete(&master_bias); \
    fors_setting_delete(&setting); \
    delete_test_files(sky_flat_filename);\
    delete_test_file(master_name );\
    delete_test_file_from_tag(MASTER_SKY_FLAT_IMG);\
} while(0)

/**
 * @brief   Test sky flat recipe
 */
static void
test_img_sky_flat(void)
{
    /* Input */
    cpl_frameset *frames = cpl_frameset_new();
    cpl_parameterlist *parameters = cpl_parameterlist_new();

    /* Output */
    fors_image *raw_sflat = NULL;
    fors_image *master_sflat = NULL;
    fors_image *master_bias = NULL;

    fors_setting *setting = NULL;

    /* Simulate data */
    const char *sky_flat_filename[] = {"img_sky_flat_1.fits",
                                       "img_sky_flat_2.fits",
                                       "img_sky_flat_3.fits"};
    const char* master_name = "img_sky_flat_master_bias.fits";


    double sky_flat_exptime[] = {1, 2, 5};
    {
        unsigned i;
        
        for (i = 0; i < sizeof(sky_flat_filename)/sizeof(char *); i++) {
            cpl_frame *sflat = create_sky_flat(sky_flat_filename[i],
                                               SKY_FLAT_IMG, CPL_FRAME_GROUP_RAW,
                                               sky_flat_exptime[i]);

            cpl_frame_set_group(sflat, CPL_FRAME_GROUP_RAW);
            cpl_frameset_insert(frames, sflat);
        }
    }
    
    setting = fors_setting_new(cpl_frameset_get_position(frames, 0));
    
    cpl_frameset_insert(frames, 
                        create_master_bias(master_name,
                                    MASTER_BIAS, CPL_FRAME_GROUP_CALIB));

    fors_img_sky_flat_define_parameters(parameters);
    assure( !cpl_error_get_code(), return, 
            "Create parameters failed");
    
    fors_parameterlist_set_defaults(parameters);

    /* Call recipe */
    fors_img_sky_flat(frames, parameters);
    assure( !cpl_error_get_code(), return, 
            "Execution error");

    /* Test results */

    /* Existence */
    const char *const product_tags[] = {MASTER_SKY_FLAT_IMG};
    const char *const qc[] = {"QC OVEREXPO"};
    test_recipe_output(frames,
                       product_tags, sizeof product_tags / sizeof *product_tags,
		       MASTER_SKY_FLAT_IMG,
                       qc, sizeof qc / sizeof *qc);
    
    /* Numbers */
    {
        /* New and previous frames */
        test( cpl_frameset_find(frames, MASTER_BIAS) != NULL );
        test( cpl_frameset_find(frames, SKY_FLAT_IMG) != NULL );
        
        master_sflat = fors_image_load(
            cpl_frameset_find(frames, MASTER_SKY_FLAT_IMG));
        
        master_bias  = fors_image_load(
            cpl_frameset_find(frames, MASTER_BIAS));
        
        raw_sflat    = fors_image_load(
            cpl_frameset_find(frames, SKY_FLAT_IMG));
        
        /* Verify that relative error decreased  */
        test( fors_image_get_error_mean(master_sflat, NULL) /
              fors_image_get_mean(master_sflat, NULL) 
              <
              fors_image_get_error_mean(raw_sflat, NULL) /
              fors_image_get_mean(raw_sflat, NULL));

        /* Verify normalization */
        test_rel( fors_image_get_mean(master_sflat, NULL),
                  1.0, 0.01);
    }

    cleanup;
    return;
}

/**
 * @brief   Test of image module
 */
int main(void)
{
    TEST_INIT;
    
    test_img_sky_flat();

    TEST_END;
}

/**@}*/
