/* $Id: fors_bias-test.c,v 1.9 2013-04-25 10:00:19 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-04-25 10:00:19 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_bias_impl.h>
#include <fors_dfs.h>
#include <fors_utils.h>

#include <test_simulate.h>
#include <test.h>

#include <cpl.h>

#include <fs_test.h>

/**
 * @defgroup fors_bias_test  test of master bias recipe
 */

/**@{*/


#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    fors_setting_delete(&setting); \
    fors_image_delete(&raw_bias); \
    fors_image_delete(&master_bias); \
    cpl_propertylist_delete(product_header); \
    delete_test_files(bias_filename);\
    delete_test_files_from_tag(product_tags);\
} while(0)

/**
 * @brief   Test bias recipe
 */
static void
test_bias(void)
{
    /* Input */
    cpl_frameset *frames          = cpl_frameset_new();
    cpl_parameterlist *parameters = cpl_parameterlist_new();

    /* Output */
    fors_image *master_bias  = NULL;
    fors_image *raw_bias  = NULL;
    cpl_propertylist *product_header = NULL;

    fors_setting *setting = NULL;

    /* Simulate data */
    const char *bias_filename[] = {"bias_1.fits",
                                   "bias_2.fits",
                                   "bias_3.fits",
                                   "bias_4.fits",
                                   "bias_5.fits"};
    const char *product_tags[] = {MASTER_BIAS};

    {
        unsigned i;
        
        for (i = 0; i < sizeof(bias_filename)/sizeof(char *); i++) {
            cpl_frameset_insert(frames, 
                                create_bias(bias_filename[i],
                                            BIAS, CPL_FRAME_GROUP_RAW));
        }
    }

    setting = fors_setting_new(cpl_frameset_get_position(frames, 0));

    fors_bias_define_parameters(parameters);
    assure( !cpl_error_get_code(), return, 
            "Create parameters failed");
    
    fors_parameterlist_set_defaults(parameters);

    /* Call recipe */
    fors_bias(frames, parameters);
    assure( !cpl_error_get_code(), return, 
            "Execution error");

    /* Test existence of QC + products */
    const char *const qc[] = 
        {"QC BIAS STRUCT", "QC BIAS LEVEL", "QC RON", "QC BIAS FPN",
         "QC MBIAS LEVEL",
         "QC MBIAS RONEXP", "QC MBIAS NOISE", "QC MBIAS NRATIO", "QC MBIAS STRUCT"};
    test_recipe_output(frames, 
                       product_tags, sizeof product_tags / sizeof *product_tags,
		       MASTER_BIAS,
                       qc, sizeof qc / sizeof *qc);
    
    /* Test results */
    {
        /* New and previous frames */
        test( cpl_frameset_find(frames, BIAS) != NULL );

        master_bias = fors_image_load(
            cpl_frameset_find(frames, MASTER_BIAS));
        
        raw_bias    = fors_image_load(
            cpl_frameset_find(frames, BIAS));
        
        /* Verify that relative error decreased  */
        /* 200 is the simulated overscan value. The master bias no longer
         * has overscan */
        test( fors_image_get_error_mean(master_bias, NULL) /
              (fors_image_get_mean(master_bias, NULL) + 200)
              <
              fors_image_get_error_mean(raw_bias, NULL) /
              fors_image_get_mean(raw_bias, NULL));


        /* QC numbers */
        product_header = 
            cpl_propertylist_load(cpl_frame_get_filename(
                                      cpl_frameset_find(frames,
                                                        MASTER_BIAS)),
                                  0);
        assure( product_header != NULL, return, NULL );

        test_abs( cpl_propertylist_get_double(product_header,
                                              "ESO QC BIAS LEVEL"),
                  fors_image_get_median(raw_bias, NULL), 0.01 );
        
        test_abs( cpl_propertylist_get_double(product_header,
                                              "ESO QC MBIAS LEVEL"),
                  fors_image_get_median(master_bias, NULL), 0.01 );
        
        test_rel( cpl_propertylist_get_double(product_header,
                                              "ESO QC RON"),
                  fors_image_get_stdev(raw_bias, NULL), 0.10 );

        /* fixed pattern and structure should be smaller than RON */
        cpl_test_leq(cpl_propertylist_get_double(product_header,"ESO QC BIAS FPN"),
                  cpl_propertylist_get_double(product_header,"ESO QC RON"));

        cpl_test_leq( cpl_propertylist_get_double(product_header,"ESO QC BIAS STRUCT"),
                  cpl_propertylist_get_double(product_header,"ESO QC RON"));
    }

    cleanup;
    return;
}


/**
 * @brief   Test of image module
 */
int main(void)
{
    TEST_INIT;
    
    test_bias();

    TEST_END;
}

/**@}*/
