/* $Id: test_file_deletion.h,v 1.0 2017-01-19 11:16:00 msalmist Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: msalmist $
 * $Date: 2017-01-20 10:44:58 $
 * $Revision: 1.0 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_TESTS_FS_TEST_H_
#define FORS_TESTS_FS_TEST_H_

#include <stdlib.h>

void cleanup_files(const char* fnames[], size_t n_files);

#define delete_test_files(fnames)\
	cleanup_files(fnames, sizeof(fnames)/sizeof(fnames[0]));\

void delete_test_file(const char* fname);

void cleanup_tags(const char* tags[], size_t n_files);

#define delete_test_files_from_tag(tags)\
		cleanup_tags(tags, sizeof(tags)/sizeof(tags[0]));\

void delete_test_file_from_tag(const char* tag);

#endif /* FORS_TESTS_FS_TEST_H_ */
