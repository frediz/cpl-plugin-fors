/* $Id: fors_zeropoint-test.c,v 1.18 2011-07-19 15:49:52 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2011-07-19 15:49:52 $
 * $Revision: 1.18 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_zeropoint_impl.h>
#include <fors_dfs.h>
#include <fors_utils.h>

#include <fs_test.h>
#include <test_simulate.h>
#include <test.h>

/**
 * @defgroup fors_zeropoint_test  zeropoint recipe tests
 */

/**@{*/


#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    delete_test_file(std_img_fname);\
    delete_test_file(master_b_fname);\
    delete_test_file(master_s_f_fname);\
    delete_test_file(cat_fname);\
    delete_test_file(phot_table);\
} while(0)

/**
 * @brief  Test zeropoint recipe
 */
static void
test_zeropoint(void)
{
    /* Input */
    cpl_frameset      *frames     = cpl_frameset_new();
    cpl_parameterlist *parameters = cpl_parameterlist_new();
    cpl_parameter     *p          = NULL;
    double exptime = 1.0;

    const char* std_img_fname = "zeropoint_standard_img.fits";
    const char* master_b_fname = "zeropoint_master_bias.fits";
    const char* master_s_f_fname = "zeropoint_master_sky_flat.fits";
    const char* cat_fname = "zeropoint_std_cat.fits";
    const char* phot_table = "zeropoint_phot_table.fits";
    /* Products */
    
    /* Simulate data */
    cpl_frameset_insert(frames, create_standard(std_img_fname ,
                                                STANDARD_IMG,
                                                CPL_FRAME_GROUP_RAW));
    cpl_frameset_insert(frames, create_bias(master_b_fname,
                                            MASTER_BIAS,
                                            CPL_FRAME_GROUP_CALIB));
    cpl_frameset_insert(frames, create_sky_flat(master_s_f_fname ,
                                                MASTER_SKY_FLAT_IMG,
                                                CPL_FRAME_GROUP_CALIB, exptime));
    cpl_frameset_insert(frames, create_std_cat(cat_fname,
                                               FLX_STD_IMG,
                                               CPL_FRAME_GROUP_CALIB));
    cpl_frameset_insert(frames, create_phot_table(phot_table,
                                                  PHOT_TABLE,
                                                  CPL_FRAME_GROUP_CALIB));
    
    /* Set parameters */
    fors_zeropoint_define_parameters(parameters);
    p = cpl_parameter_new_enum("fors.fors_zeropoint.extract_method",
                               CPL_TYPE_STRING,
                               "Source extraction method",
                               "fors.fors_zeropoint",
                               "sex", 2,
                               "sex", "test");
    cpl_parameterlist_append(parameters, p);
    
    assure( !cpl_error_get_code(), return, 
            "Create parameters failed");
    
    fors_parameterlist_set_defaults(parameters);
 
    /* Do not rely on SExtractor for this unit test */
    cpl_parameter_set_string(cpl_parameterlist_find(parameters,
                                                    "fors.fors_zeropoint.extract_method"),
                             "test");

    /* Note: the extracted source positions do not match the catalogue,
       but the recipe will find a solution anyway by increasing the
       search radius. Suppress warnings */
    cpl_msg_severity before = cpl_msg_get_level();
    cpl_msg_set_level(CPL_MSG_ERROR);

// Disable test until pattern-matching vs robust shift is decided.
cleanup;
return;

    fors_zeropoint(frames, parameters);

    cpl_msg_set_level(before);



    /* Test existence of QC + products */
    const char *const product_tags[] = {SOURCES_STD, 
                                        ALIGNED_PHOT, 
                                        STANDARD_REDUCED_IMG, 
                                        PHOT_BACKGROUND_STD_IMG};
    const char *const qc[] = 
        {"QC ZPOINT", "QC ZPOINTRMS", "QC ZPOINT NSTARS",
         "QC EXTCOEFF"};
    const char *main_product = ALIGNED_PHOT;

    test_recipe_output(frames, 
                       product_tags, sizeof product_tags / sizeof *product_tags,
		       main_product,
                       qc, sizeof qc / sizeof *qc);
        
    cleanup;
    return;
}


/**
 * @brief   Test of zeropoint recipe
 */
int main(void)
{
    TEST_INIT;

    /* cpl_msg_set_level(CPL_MSG_DEBUG); */
    test_zeropoint();

    TEST_END;
}

/**@}*/
