/* $Id: fors-test.c,v 1.2 2007-07-09 06:59:34 jmlarsen Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-07-09 06:59:34 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_utils.h>
#include <test.h>

/**
 * @defgroup fors_test  Basic library test
 */

/**@{*/

/**
 * @brief   Check version number
 *
 * This is a simple test to verify that we are linking to the correct
 * libraries
 */
static void
test_version(void)
{
    test_eq(FORS_BINARY_VERSION, fors_get_version_binary());
}


/**
 * @brief   Generic library tests
 */
int main(void)
{
    TEST_INIT;

    test_version();

    TEST_END;
}

/**@}*/
