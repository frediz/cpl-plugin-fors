/* $Id: fors_star-test.c,v 1.2 2007-10-17 09:17:41 jmlarsen Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-10-17 09:17:41 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_star.h>
#include <fors_utils.h>

#include <test_simulate.h>
#include <test.h>

/**
 * @defgroup fors_star_test  Star tests
 */

/**@{*/

#undef cleanup
#define cleanup \
do { \
    fors_star_delete(&s); \
    fors_star_delete(&t); \
} while (0)
/**
 * @brief  
 */
static void
test_star(void)
{
    double x = 110;
    double y = 399;
    double smajor = 3;
    double sminor = 2;
    double fwhm = 3.5;
    double theta = -0.1;
    double m = -17;
    double dm = 0.05;
    double si = 1.0;

    fors_star *s = fors_star_new(x, y,
                                 fwhm,
                                 smajor, sminor,
                                 theta,
                                 m, dm, si);

    fors_star *t = fors_star_duplicate(s);

    test( fors_star_equal(s, t) );

    test_abs( fors_star_distsq(s, t), 0, 0.001 );

    cleanup;
    return;
}


/**
 * @brief   Test of star module
 */
int main(void)
{
    TEST_INIT;

    test_star();

    TEST_END;
}

/**@}*/
