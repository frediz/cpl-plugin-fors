/* $Id: fors_dark-test.c,v 1.6 2013-09-11 13:47:53 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-09-11 13:47:53 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fors_dark_impl.h>
#include <fors_dfs.h>
#include <fors_utils.h>

#include <test_simulate.h>
#include <test.h>

#include <cpl.h>

#include <fs_test.h>

#include <string.h>


/**
 * @defgroup fors_dark_test  test of master dark recipe
 */

/**@{*/


/**
 * @brief Internal utility functions
 */
static inline double
get_stddevs_from_raws(const cpl_frame ** frames){

    fors_image * img1 = fors_image_load(frames[0]);
    fors_image * img2 = fors_image_load(frames[1]);
    fors_image * img3 = fors_image_load(frames[2]);

    double m = fors_image_get_stdev(img1, NULL);
    m += fors_image_get_stdev(img2, NULL);
    m += fors_image_get_stdev(img3, NULL);

    m /= 3.0;

    fors_image_delete(&img1);
    fors_image_delete(&img2);
    fors_image_delete(&img3);

    return m;
}


static inline double
get_mean_from_raws(const cpl_frame ** frames){

    fors_image * img1 = fors_image_load(frames[0]);
    fors_image * img2 = fors_image_load(frames[1]);
    fors_image * img3 = fors_image_load(frames[2]);

    double m = fors_image_get_mean(img1, NULL);
    m += fors_image_get_mean(img2, NULL);
    m += fors_image_get_mean(img3, NULL);

    m /= 3.0;

    fors_image_delete(&img1);
    fors_image_delete(&img2);
    fors_image_delete(&img3);

    return m;
}

static inline double
get_median_from_raws(const cpl_frame ** frames){

    fors_image * img1 = fors_image_load(frames[0]);
    fors_image * img2 = fors_image_load(frames[1]);
    fors_image * img3 = fors_image_load(frames[2]);
    cpl_vector * v = cpl_vector_new(3);

    cpl_vector_set(v, 0, fors_image_get_mean(img1, NULL));
    cpl_vector_set(v, 1, fors_image_get_mean(img2, NULL));
    cpl_vector_set(v, 2, fors_image_get_mean(img3, NULL));

    fors_image_delete(&img1);
    fors_image_delete(&img2);
    fors_image_delete(&img3);

    double m = cpl_vector_get_median(v);
    cpl_vector_delete(v);

    return m;
}

#undef cleanup
#define cleanup \
do { \
    cpl_frameset_delete(frames); \
    cpl_parameterlist_delete(parameters); \
    fors_image_delete(&raw_dark); \
    fors_image_delete(&master_dark); \
    fors_image_delete(&master_bias); \
    fors_setting_delete(&setting); \
    delete_test_files(dark_filename);\
    delete_test_file(dark_master_filename);\
    delete_test_file("master_dark.fits");\
} while(0)

/**
 * @brief   Test dark recipe
 */
static void
test_dark(void)
{
    /* Input */
    cpl_frameset *frames          = cpl_frameset_new();
    cpl_parameterlist *parameters = cpl_parameterlist_new();

    /* Output */
    fors_image *master_dark  = NULL;
    fors_image *master_bias  = NULL;
    fors_image *raw_dark  = NULL;

    fors_setting *setting = NULL;

    /* Simulate data */
    const char *dark_filename[] = {"dark_1.fits",
                                   "dark_2.fits",
                                   "dark_3.fits"};
    const cpl_frame * darks[3] = {NULL};
    const char* dark_master_filename = "dark_master_bias.fits";

    {
        unsigned i;
        
        for (i = 0; i < sizeof(dark_filename)/sizeof(char *); i++) {

            cpl_frame * f = create_dark(dark_filename[i],
                    DARK, CPL_FRAME_GROUP_RAW);

            cpl_frameset_insert(frames, 
                                f);

            darks[i] = f;
        }
    }

    setting = fors_setting_new(cpl_frameset_get_position(frames, 0));

    cpl_frameset_insert(frames, 
                        create_master_bias(dark_master_filename,
                                    MASTER_BIAS, CPL_FRAME_GROUP_CALIB));
    
    fors_dark_define_parameters(parameters);
    assure( !cpl_error_get_code(), return, 
            "Create parameters failed");
    
    fors_parameterlist_set_defaults(parameters);

    /* Call recipe */
    fors_dark(frames, parameters);
    assure( !cpl_error_get_code(), return, 
            "Execution error");

    /* Test results */
    {
        /* New and previous frames */
        test( cpl_frameset_find(frames, MASTER_DARK) != NULL );
        test( cpl_frameset_find(frames, MASTER_BIAS) != NULL );
        test( cpl_frameset_find(frames, DARK) != NULL );
        
        master_dark = fors_image_load(
            cpl_frameset_find(frames, MASTER_DARK));
        
        master_bias = fors_image_load(
            cpl_frameset_find(frames, MASTER_BIAS));
        
        raw_dark    = fors_image_load(
            cpl_frameset_find(frames, DARK));
        
        /* Verify relation
           master_dark = raw_dark - master_bias */
        test_rel( fors_image_get_mean(master_dark, NULL),
                  fors_image_get_mean(raw_dark, NULL) -
                  fors_image_get_mean(master_bias, NULL) - 200, //Hardcoded overscan level
                  0.03);

        /* Verify that error decreased  */
        {
            test( fors_image_get_error_mean(master_dark, NULL) /
                  (fors_image_get_mean(master_dark, NULL) +
                   fors_image_get_mean(master_bias, NULL) + 200) //Hardcoded overscan level
                  <
                  fors_image_get_error_mean(raw_dark, NULL) /
                  fors_image_get_mean(raw_dark, NULL));
        }
        /* Verify QC Parameters */
        {
            const char * master_dark_fname =
                    cpl_frame_get_filename(cpl_frameset_find(frames, MASTER_DARK));

            test(master_dark_fname != NULL && strlen(master_dark_fname) > 0);

            cpl_propertylist * list = cpl_propertylist_load(master_dark_fname, 0);
            test(list != NULL);
            test(cpl_error_get_code() == CPL_ERROR_NONE);


            const double raw_mean_of_means = cpl_propertylist_get_double(list,
                    "ESO QC DARK RAW MEAN MEAN");
            test_rel(raw_mean_of_means, get_mean_from_raws(darks), 1e-10);

            test(cpl_error_get_code() == CPL_ERROR_NONE);
            const double raw_median_of_means = cpl_propertylist_get_double(list,
                    "ESO QC DARK RAW MEAN MEDIAN");
            test_rel(raw_median_of_means, get_median_from_raws(darks), 1e-10);

            test(cpl_error_get_code() == CPL_ERROR_NONE);
            const double raw_mean_of_stddevs = cpl_propertylist_get_double(list,
                    "ESO QC DARK RAW STDEV MEAN");
            test(cpl_error_get_code() == CPL_ERROR_NONE);
            test_rel(raw_mean_of_stddevs, get_stddevs_from_raws(darks), 1e-10);



            const long long master_n_badpix = cpl_propertylist_get_long_long(list,
                        "ESO QC DARK NBADPX");
            test(cpl_error_get_code() == CPL_ERROR_NONE);
            test(master_n_badpix ==  cpl_image_count_rejected(master_dark->data));

            double master_median = cpl_propertylist_get_double(list,
                        "ESO QC DARK MEDIAN");
            test(cpl_error_get_code() == CPL_ERROR_NONE);
            test_rel(master_median, cpl_image_get_median(master_dark->data), 1e-10);

            double master_mean = cpl_propertylist_get_double(list,
                        "ESO QC DARK MEAN");
            test(cpl_error_get_code() == CPL_ERROR_NONE);
            test_rel(master_mean, cpl_image_get_mean(master_dark->data), 1e-10);

            double master_stddev = cpl_propertylist_get_double(list,
                        "ESO QC DARK STDEV");
            test(cpl_error_get_code() == CPL_ERROR_NONE);
            test_rel(master_stddev, cpl_image_get_stdev(master_dark->data), 1e-10);

            double master_min = cpl_propertylist_get_double(list,
                        "ESO QC DARK MIN");
            test(cpl_error_get_code() == CPL_ERROR_NONE);
            test_rel(master_min, cpl_image_get_min(master_dark->data), 1e-10);

            double master_max = cpl_propertylist_get_double(list,
                        "ESO QC DARK MAX");
            test(cpl_error_get_code() == CPL_ERROR_NONE);
            test_rel(master_max, cpl_image_get_max(master_dark->data), 1e-10);

            cpl_propertylist_delete(list);
        }
    }

    cleanup;
    return;
}

/**
 * @brief   Test of image module
 */
int main(void)
{
    TEST_INIT;
    
    test_dark();

    TEST_END;
}

/**@}*/
