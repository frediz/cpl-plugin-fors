/*
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "fors_preprocess.h"
#include "fiera_config.h"
#include "fors_detmodel.h"
#include "fors_ccd_config.h"
#include "fors_overscan.h"
#include "fors_bpm.h"

/**
 * 
 */
fors_image *
fors_image_load_preprocess(const cpl_frame *frame,
                           const cpl_frame *bias_frame)
{
    cpl_errorstate      error_prevstate = cpl_errorstate_get();
    
    /* Load the image */
    fors_image * image_raw = fors_image_load(frame);
    if (!image_raw)
        return NULL;
    
    /* Read the CCD configuration */
    const char * filename = cpl_frame_get_filename(frame);
    cpl_propertylist * header = cpl_propertylist_load(filename, 0) ;
    fors::fiera_config ccd_config(header);
    if(!cpl_errorstate_is_equal(error_prevstate))
        return NULL;
    
    /* Update RON estimation from bias */
    cpl_propertylist * master_bias_header =
       cpl_propertylist_load(cpl_frame_get_filename(bias_frame), 0);
    fors::update_ccd_ron(ccd_config, master_bias_header);
    if(!cpl_errorstate_is_equal(error_prevstate))
        return NULL;
    
    /* Check that the overscan configuration is consistent */
    bool perform_preoverscan = !fors_is_preoverscan_empty(ccd_config);
    if(perform_preoverscan != 
           fors_is_master_bias_preoverscan_corrected(master_bias_header))
    {
        cpl_error_set_message(cpl_func, CPL_ERROR_INCOMPATIBLE_INPUT,
                "Master bias overscan configuration doesn't match science");
        return NULL;
    }
    cpl_propertylist_delete(master_bias_header);

    /* Create variances map */
    std::vector<double> overscan_levels; 
    if(perform_preoverscan)
        overscan_levels = fors_get_bias_levels_from_overscan(image_raw, 
                                                             ccd_config);
    else
    {
        fors_image *bias_img = fors_image_load(bias_frame);
        overscan_levels = fors_get_bias_levels_from_mbias(bias_img, 
                                                          ccd_config);
        fors_image_delete(&bias_img);
    }
    fors_image_variance_from_detmodel(image_raw, ccd_config, overscan_levels);
    if(!cpl_errorstate_is_equal(error_prevstate))
        return NULL;

    /* Subtract overscan */
    fors_image * image_preproc;
    if(perform_preoverscan)
        image_preproc = fors_subtract_prescan(image_raw, ccd_config);
    else 
    {
        image_preproc = fors_image_duplicate(image_raw); 
        //The rest of the recipe assumes that the images carry a bpm.
        fors_bpm_image_make_explicit(image_preproc); 
    }
    if(!cpl_errorstate_is_equal(error_prevstate))
        return NULL;

    /* Trimm pre/overscan */
    if(perform_preoverscan)
        fors_trimm_preoverscan(image_preproc, ccd_config);
    fors_image_delete(&image_raw);
    if(!cpl_errorstate_is_equal(error_prevstate))
        return NULL;

    return image_preproc;
}


/**@}*/
