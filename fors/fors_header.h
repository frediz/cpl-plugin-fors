/* $Id: fors_header.h,v 1.1 2012-11-05 17:11:40 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2012-11-05 17:11:40 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_HEADER_H
#define FORS_HEADER_H

#include <cpl.h>

CPL_BEGIN_DECLS

cpl_error_code fors_header_write_string(cpl_propertylist *header,
                                        const char *name, const char *value, 
                                        const char *comment);
cpl_error_code fors_header_write_int(cpl_propertylist *header, int value,
                                     const char *name, const char *unit,
                                     const char *comment);
cpl_error_code fors_header_write_double(cpl_propertylist *header, double value, 
                                        const char *name, const char *unit, 
                                        const char *comment);

CPL_END_DECLS

#endif /* FORS_HEADER_H */
