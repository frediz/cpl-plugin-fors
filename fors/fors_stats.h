/* $Id: fors_stats.h,v 1.10 2017-02-16 09:00:00 msalmist Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2017 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: msalmist $
 * $Date: 2017-02-16 09:00:00 $
 * $Revision: 1.00 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_STATS_H
#define FORS_STATS_H

#include <fors_image.h>

#include <cpl.h>

CPL_BEGIN_DECLS

void fors_write_max_in_propertylist(fors_image *image, cpl_propertylist *l,
                                    const char* tag);

void fors_write_min_in_propertylist(fors_image *image, cpl_propertylist *l,
                                    const char* tag);

void fors_write_mean_in_propertylist(fors_image *image, cpl_propertylist *l,
                                    const char* tag);

void fors_write_stdev_in_propertylist(fors_image *image, cpl_propertylist *l,
                                    const char* tag);

void fors_write_median_in_propertylist(fors_image *image, cpl_propertylist *l,
                                    const char* tag);

void fors_write_num_bad_pixels_propertylist(fors_image *image,
                                            cpl_propertylist *l,
                                            const char* tag);

void fors_write_images_mean_mean_in_propertylist(fors_image_list *images,
                                          cpl_propertylist *l,
                                          const char* tag);

void fors_write_images_median_mean_in_propertylist(fors_image_list *images,
                                          cpl_propertylist *l,
                                          const char* tag);

void fors_write_images_mean_stddev_in_propertylist(fors_image_list *images,
                                          cpl_propertylist *l,
                                          const char* tag);
CPL_END_DECLS

#endif
