/* $Id: fors_header.c,v 1.1 2012-11-05 17:11:56 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2012-11-05 17:11:56 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <cpl.h>
#include <fors_utils.h>
#include <fors_paf.h>
#include <fors_dfs.h>
#include <fors_qc.h>

#define DICT_LINE_LENGTH    (80)
#define MAX_PAF_NAME_LENGTH (80)
#define PAF_ROOT_NAME       "qc"

/**
 * @defgroup forsqc Quality Control Utilities
 *
 * The module collects utility functions for quality control operations.
 */

/**@{*/

/**
 * @brief
 *   Write a string value to the active QC1 PAF object and to a header.
 *
 * @return @c CPL_ERROR_NONE on success
 *
 * @param header     Product header
 * @param name       QC1 PAF entry name.
 * @param value      Value to write.
 * @param unit       Optional unit to be associated to value.
 * @param comment    Comment to be associated to value.
 * @param instrument Instrument name
 *
 *   An entry with the specified @em name is written to the current QC1 PAF
 *   object. From the entry @em name, the name of the QC keyword that
 *   should be written to header is derived prepending the string "ESO "
 *   and replacing all '.' with a blank (e.g., "QC.BIAS.MASTER.MEAN"
 *   becomes "ESO QC BIAS MASTER MEAN"). Finally, the new keyword
 *   is written to the header. Note that before calling this funtion
 *   a QC1 PAF object must be created with a call to fors_qc_start_group().
 */
cpl_error_code fors_header_write_string(cpl_propertylist *header,
                                       const char *name, const char *value, 
                                       const char *comment)
{
    char *header_name;
    int   i;

    header_name = cpl_malloc((strlen(name) + 6) * sizeof(char *));

    strcpy(header_name, "ESO ");
    strcat(header_name, name);

    for (i = 0; header_name[i] != '\0'; i++)
        if (header_name[i] == '.')
            header_name[i] = ' ';

    if (cpl_propertylist_update_string(header, header_name, value)) {
        cpl_free(header_name);
        cpl_error_set_where(cpl_func);
        return cpl_error_get_code();
    }

    cpl_propertylist_set_comment(header, header_name, comment);

    cpl_free(header_name);

    return CPL_ERROR_NONE;
}

/**
 * @brief
 *   Write an integer value to the active QC1 PAF object and to a header.
 *
 * @return @c CPL_ERROR_NONE on success
 *
 * @param header     Product header
 * @param value      Value to write.
 * @param name       QC1 PAF entry name.
 * @param unit       Optional unit to be associated to value.
 * @param comment    Comment to be associated to value.
 * @param instrument Instrument name
 *
 *   This function writes the header entries directly to the header
 *   of the FITS file written to disk, using the qfits_replace_card() call.
 *   An entry with the specified @em name is written to the current QC1 PAF
 *   object. From the entry @em name, the name of the QC keyword that
 *   should be written to header is derived prepending the string "ESO "
 *   and replacing all '.' with a blank (e.g., "QC.BIAS.MASTER.MEAN"
 *   becomes "ESO QC BIAS MASTER MEAN"). Finally, the new keyword
 *   is written to the header. Note that before calling this funtion
 *   a QC1 PAF object must be created with a call to fors_qc_start_group().
 */

cpl_error_code fors_header_write_double(cpl_propertylist *header, double value, 
                                      const char *name, const char *unit, 
                                      const char *comment)
{

  char *header_name;
  int   i;
  char *allComment;

  allComment = cpl_malloc(81 * sizeof(char *));

  if (unit)
    snprintf(allComment, 80, "%s [%s]", comment, unit);
  else
    snprintf(allComment, 80, "%s", comment);

  
  header_name = cpl_malloc((strlen(name) + 6) * sizeof(char *));

  strcpy(header_name, "ESO ");
  strcat(header_name, name);

  for (i = 0; header_name[i] != '\0'; i++)
    if (header_name[i] == '.')
      header_name[i] = ' ';

  if (cpl_propertylist_update_double(header, header_name, value)) {
      cpl_free(header_name);
      cpl_error_set_where(cpl_func);
      return cpl_error_get_code();
  }

  cpl_propertylist_set_comment(header, header_name, allComment);

  cpl_free(header_name);
  cpl_free(allComment);

  return CPL_ERROR_NONE;

}


cpl_error_code fors_header_write_int(cpl_propertylist *header, int value,
                                     const char *name, const char *unit,
                                     const char *comment)
{

  char *header_name;
  int   i;
  char *allComment;

  allComment = cpl_malloc(81 * sizeof(char *));

  if (unit)
    snprintf(allComment, 80, "%s [%s]", comment, unit);
  else
    snprintf(allComment, 80, "%s", comment);

  header_name = cpl_malloc((strlen(name) + 6) * sizeof(char *));

  strcpy(header_name, "ESO ");
  strcat(header_name, name);

  for (i = 0; header_name[i] != '\0'; i++)
    if (header_name[i] == '.')
      header_name[i] = ' ';

  if (cpl_propertylist_update_int(header, header_name, value)) {
      cpl_free(header_name);
      cpl_error_set_where(cpl_func);
      return cpl_error_get_code();
  }

  cpl_propertylist_set_comment(header, header_name, allComment);

  cpl_free(header_name);
  cpl_free(allComment);

  return CPL_ERROR_NONE;

}

/**@}*/
