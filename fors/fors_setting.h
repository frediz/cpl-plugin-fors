/* $Id: fors_setting.h,v 1.16 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.16 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_SETTING_H
#define FORS_SETTING_H

#include <cpl.h>

/* deprecate this, only kept for now for
 * create_phot.c, fors_data.c, test_simulate.c */
    enum filter
    {
        FILTER_U, 
        FILTER_B, 
        FILTER_G,
        FILTER_V, 
        FILTER_R, 
        FILTER_I, 
        FILTER_Z
    };
    #define FORS_NUM_FILTER 7

typedef struct _fors_setting {

    int         binx,
                biny;

    int         prescan_x,
                prescan_y;   /* prescan width in 
				   software (binned) pixels */
    
/* not used for now
   int ccd_x, ccd_y;  / * CCD size in
			  physical pixels,
			  not FITS pixels 
		       */
    char        filterband;

    const char  *filter_name; /* NULL if no filter */

    double      exposure_time;  /* seconds */

    double      average_gain;  /* average ESO gain of all read-out ports */

    double      ron;    /* average read-out-noise (ADU) of all read-out ports */

    double      pixel_scale;  /*  arcsec/pixel, positive */

    const char  *read_clock;
    const char  *chip_id;

    const char  *instrument;  /* e.g. fors1 */
    const char  *version;     /* e.g. fors1/4.0.0 */
    
} fors_setting;

CPL_BEGIN_DECLS 

fors_setting *
fors_setting_new(const cpl_frame *raw);

void
fors_setting_verify(const fors_setting *ref_setting, const cpl_frame *frame,
		    fors_setting **setting);

void fors_setting_delete(fors_setting **s);

CPL_END_DECLS 
#endif
