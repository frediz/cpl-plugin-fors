/* $Id: fors_dfs.h,v 1.35 2013-09-10 19:27:01 cgarcia Exp $
 *
 * This file is part of the FORS Pipeline
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-09-10 19:27:01 $
 * $Revision: 1.35 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_DFS_H
#define FORS_DFS_H

#include <fors_image.h>

#include <cpl.h>

#include <fors_dfs_idp.h>

CPL_BEGIN_DECLS

/* Types of data to save */
enum _fors_type_ {

    FORS_TYPE_IMAGE          = 1 << 0,
    FORS_TYPE_IMAGE_ERR      = 1 << 1,
    FORS_TYPE_TABLE          = 1 << 2
};

typedef enum _fors_type_ fors_type;


/* Raw frames */
#define BIAS                    "BIAS"
#define DARK                    "DARK"
#define SCREEN_FLAT_IMG         "SCREEN_FLAT_IMG"
#define SKY_FLAT_IMG            "SKY_FLAT_IMG"
#define STANDARD_IMG            "STANDARD_IMG"
#define SCIENCE_IMG             "SCIENCE_IMG"

/* Pipeline products */
#define MASTER_BIAS             "MASTER_BIAS"
#define MASTER_DARK             "MASTER_DARK"
#define MASTER_SCREEN_FLAT_IMG  "MASTER_SCREEN_FLAT_IMG"
#define MASTER_NORM_FLAT_IMG    "MASTER_NORM_FLAT_IMG"
#define MASTER_SKY_FLAT_IMG     "MASTER_SKY_FLAT_IMG"
#define SOURCES_SCI             "SOURCES_SCI_IMG"
#define SOURCES_STD             "SOURCES_STD_IMG"
#define ALIGNED_PHOT            "ALIGNED_PHOT"
#define STANDARD_REDUCED_IMG    "STANDARD_REDUCED_IMG"
#define SCIENCE_REDUCED_IMG     "SCIENCE_REDUCED_IMG"
#define PHOTOMETRY_TABLE        "OBJECT_TABLE_SCI_IMG"
#define PHOT_BACKGROUND_SCI_IMG "PHOT_BACKGROUND_SCI_IMG"
#define PHOT_BACKGROUND_STD_IMG "PHOT_BACKGROUND_STD_IMG"
#define CORRECTION_MAP          "CORRECTION_MAP"
#define CORRECTION_FACTOR       "CORRECTION_FACTOR"
#define MASTER_FLAT_IMG         "MASTER_FLAT_IMG"
#define PHOT_COEFF_TABLE        "PHOT_COEFF_TABLE"
#define EXTINCTION_PER_NIGHT    "EXTINCTION_PER_NIGHT"
#define EXTINCTION_PER_FRAME    "EXTINCTION_PER_FRAME"
#define OFFSET_HISTOGRAM        "OFFSET_HISTOGRAM"

/* Static calibration */
#define FLX_STD_IMG             "FLX_STD_IMG"
#define PHOT_TABLE              "PHOT_TABLE"

void fors_dfs_set_groups(cpl_frameset * set);
const char *fors_dfs_pipeline_version(const cpl_propertylist *header, 
				      const char **instrument_version);
char *dfs_generate_filename(const char *);
int dfs_get_parameter_bool(cpl_parameterlist *, const char *, 
                           const cpl_table *);
int dfs_get_parameter_int(cpl_parameterlist *, const char *, 
                          const cpl_table *);
double dfs_get_parameter_double(cpl_parameterlist *, const char *, 
                                const cpl_table *);
const char *dfs_get_parameter_string(cpl_parameterlist *, const char *, 
                                     const cpl_table *);
int dfs_get_parameter_bool_const(const cpl_parameterlist *, const char *);
int dfs_get_parameter_int_const(const cpl_parameterlist *, const char *);
double dfs_get_parameter_double_const(const cpl_parameterlist *, const char *);
const char *dfs_get_parameter_string_const(const cpl_parameterlist *, const char *);
cpl_image *dfs_load_image(cpl_frameset *, const char *, cpl_type, int, int);
cpl_table *dfs_load_table(cpl_frameset *, const char *, int);
cpl_propertylist *dfs_load_header(cpl_frameset *, const char *, int);
void fors_dfs_add_wcs(cpl_propertylist *header, const cpl_frame *frame,
                      const fors_setting *setting);
void fors_dfs_add_exptime(cpl_propertylist *header, const cpl_frame *frame,
                          double exptime);
void
fors_dfs_save_image_err_idp(cpl_frameset *frameset, const fors_image *image,
                    const char *category, cpl_propertylist *header,
                    cpl_propertylist *err_header,
                    const cpl_parameterlist *parlist, const char *recipename,
                    const cpl_frame *inherit_frame,
					const fors_dfs_idp_converter * converter);

void fors_dfs_save_image_err(cpl_frameset *frameset, const fors_image *image,
             const char *category, cpl_propertylist *header,
             cpl_propertylist *err_header,
             const cpl_parameterlist *parlist, const char *recipename,
             const cpl_frame *raw_frame);
void fors_dfs_save_image_err_mask(cpl_frameset *frameset, 
             const fors_image *image, const cpl_image *mask,
             const char *category, cpl_propertylist *header,
             const cpl_parameterlist *parlist, const char *recipename,
             const cpl_frame *raw_frame);
void fors_dfs_save_image(cpl_frameset *frameset, const cpl_image *image,
             const char *category, cpl_propertylist *header,
             const cpl_parameterlist *parlist, const char *recipename,
             const cpl_frame *raw_frame);
void fors_dfs_save_image_mask(cpl_frameset *frameset, const cpl_image *image,
                         const cpl_image *mask, const char *category, 
                         cpl_propertylist *header, 
                         const cpl_parameterlist *parlist, const char *recipename,
                         const cpl_frame *inherit_frame);
void fors_dfs_save_image_wcs(cpl_frameset *frameset, const fors_image *image,
			     const char *category, cpl_propertylist *header,
			     const cpl_parameterlist *parlist, const char *recipename,
			     const cpl_frame *raw_frame);
void fors_dfs_save_table(cpl_frameset *frameset, const cpl_table *table,
			 const char *category, cpl_propertylist *header,
			 const cpl_parameterlist *parlist, const char *recipename,
			 const cpl_frame *raw_frame);
int dfs_save_image(cpl_frameset *, const cpl_image *, const char *, 
                   cpl_propertylist *, const cpl_parameterlist *, 
                   const char *, const char *) CPL_ATTR_DEPRECATED; 
int dfs_save_table(cpl_frameset *, const cpl_table *, const char *, 
                   cpl_propertylist *, const cpl_parameterlist *, 
                   const char *, const char *) CPL_ATTR_DEPRECATED;
int dfs_equal_keyword(cpl_frameset *frameset, const char *keyword);
void fors_begin(cpl_frameset *frames, const char *description_short);
int fors_end(const cpl_frameset *frames, cpl_errorstate before_exec);

cpl_error_code dfs_save_table_ext(cpl_table *, const char *,
				  cpl_propertylist *);
cpl_error_code dfs_save_image_ext(cpl_image *, const char *,
				  cpl_propertylist *);
cpl_error_code dfs_save_image_null(cpl_frameset *, cpl_parameterlist *,
				   const char *, const char *,
				   const char *);

CPL_END_DECLS

#endif   /* FORS_DFS_H */
