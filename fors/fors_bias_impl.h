/* $Id: fors_bias_impl.h,v 1.2 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_BIAS_IMPL_H
#define FORS_BIAS_IMPL_H

#include <cpl.h>
#include <fors_image.h>
#include <fors_stack.h>

CPL_BEGIN_DECLS

extern const char *const fors_bias_name;
extern const char *const fors_bias_description_short;
extern const char *const fors_bias_author;
extern const char *const fors_bias_email;
extern const char *const fors_bias_description;
void fors_bias_define_parameters(cpl_parameterlist *parameters);
fors_image *
fors_bias_stack(const fors_image_list *images, const stack_method *sm);
void fors_bias(cpl_frameset *frames, const cpl_parameterlist *parameters);

CPL_END_DECLS

#endif
