/* 
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include "fors_saturation_mos.h"
#include "calibrated_slit.h"

#include <vector>

void fors_saturation_reject_sat_slits(std::vector<mosca::image>& raw_flats,
                                 const fors::calibrated_slits& slits,
                                 cpl_mask ** non_linear_flat_masks,
                                 cpl_mask ** saturated_flat_masks,
                                 double max_nonlinear_ratio,
                                 std::vector<std::vector<double> >& slit_nonlinear_ratio,
                                 std::vector<std::vector<int> >& slit_nonlinear_count)
{
    
    /* We work on a slit per slit basis */
    size_t n_slits = slits.size();
    size_t n_flats = raw_flats.size();
    if(n_flats == 0)
        return;
    
    /* Get the masks of the valid pixels for all the slits */
    mosca::image& ref_flat = raw_flats.front(); 
    cpl_mask ** slit_masks = 
        fors::get_all_slits_valid_masks(slits, ref_flat.dispersion_axis());
    

    /* Allocate the saturation vectors */
    std::vector<int> n_slit_total(n_slits);
    slit_nonlinear_ratio.resize(n_slits);
    slit_nonlinear_count.resize(n_slits);
    for(size_t i_slit = 0; i_slit < n_slits; i_slit++)
    {
        slit_nonlinear_ratio[i_slit].resize(n_flats);
        slit_nonlinear_count[i_slit].resize(n_flats);
    }

    /* Get the saturation count for each slit & flat */
    for(size_t i_slit = 0; i_slit < n_slits; i_slit++)
    {
        n_slit_total[i_slit] = cpl_mask_count(slit_masks[i_slit]);
        for(size_t i_flat = 0; i_flat< n_flats; i_flat++)
        {
            cpl_mask * this_slit_flat = cpl_mask_duplicate(non_linear_flat_masks[i_flat]);
            cpl_mask_or(this_slit_flat, saturated_flat_masks[i_flat]);
            cpl_mask_and(this_slit_flat, slit_masks[i_slit]);
            slit_nonlinear_count[i_slit][i_flat] = cpl_mask_count(this_slit_flat);
            cpl_mask_delete(this_slit_flat);
        }
    }
    
    for(size_t i_slit = 0; i_slit < n_slits; i_slit++)
    {
        int n_bad_flats = 0;
        for(size_t i_flat = 0; i_flat< n_flats; i_flat++)
        {
            mosca::image& this_flat = raw_flats[i_flat];
            if(n_slit_total[i_slit] != 0)
                slit_nonlinear_ratio[i_slit][i_flat] = slit_nonlinear_count[i_slit][i_flat] / 
                (double)n_slit_total[i_slit];
            else 
                slit_nonlinear_ratio[i_slit][i_flat] = 0;
            if(slit_nonlinear_ratio[i_slit][i_flat] > max_nonlinear_ratio)
            {
                cpl_msg_warning(cpl_func, "Flat %zd in slit %zd (ID %d) contains "
                "too many saturated pixels (%f %%). Removing it for this slit", 
                   i_flat + 1, i_slit + 1, slits[i_slit].slit_id(), 100*slit_nonlinear_ratio[i_slit][i_flat]);
                cpl_image_reject_from_mask(this_flat.get_cpl_image(),
                                           slit_masks[i_slit]);
                n_bad_flats++;
            }

            if(n_bad_flats)
                cpl_msg_info(cpl_func, "For slit %zd, %d flats saturated",
                             i_slit, n_bad_flats);
        }
    }
    
    /* Deallocate */
    for(size_t i_slit = 0; i_slit < n_slits; i_slit++)
        cpl_mask_delete(slit_masks[i_slit]);
    cpl_free(slit_masks);
}
