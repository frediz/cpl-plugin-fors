/* $Id: fors_paf.h,v 1.2 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */
 
#ifndef FORS_PAF_H
#define FORS_PAF_H

#include <sys/types.h>

#include <cpl.h>


CPL_BEGIN_DECLS

/*
 * Maximum length of a parameter file record, i.e. maximum number of
 * characters per line of a parameter file on disk. This does not include
 * a trailing 0.
 */

#define PAF_RECORD_MAX  (256)


/*
 * PAF value types
 */

enum _FORS_PAF_TYPE_ {
    PAF_TYPE_NONE,
    PAF_TYPE_BOOL,
    PAF_TYPE_INT,
    PAF_TYPE_DOUBLE,
    PAF_TYPE_STRING
};

typedef enum _FORS_PAF_TYPE_ ForsPAFType;

/*
 * PAF object
 */

typedef struct _FORS_PAF_ ForsPAF;

/*
 * Create, copy and destroy operations
 */

ForsPAF *newForsPAF(const char *, const char *, const char *,
                          const char *);
void deleteForsPAF(ForsPAF *);

/*
 * Nonmodifying operations
 */

int forsPAFIsEmpty(const ForsPAF *);
size_t forsPAFGetSize(const ForsPAF *);
int forsPAFContains(const ForsPAF *, const char *);
size_t forsPAFCount(const ForsPAF *, const char *);

/*
 * Header operations
 */

ForsPAFType forsPAFType(const ForsPAF *, const char *);

const char *forsPAFGetName(const ForsPAF *);
const char *forsPAFGetTag(const ForsPAF *);
const char *forsPAFGetId(const ForsPAF *);
const char *forsPAFGetDescription(const ForsPAF *);

int forsPAFSetName(ForsPAF *, const char *);
int forsPAFSetTag(ForsPAF *, const char *);
int forsPAFSetId(ForsPAF *, const char *);
int forsPAFSetDescription(ForsPAF *, const char *);

int forsPAFSetHeader(ForsPAF *, const char *, const char *, const char *,
                    const char *);

/*
 * Element access
 */

int forsPAFGetValueBool(const ForsPAF *, const char *);
int forsPAFGetValueInt(const ForsPAF *, const char *);
double forsPAFGetValueDouble(const ForsPAF *, const char *);
const char *forsPAFGetValueString(const ForsPAF *, const char *);
const char *forsPAFGetComment(const ForsPAF *, const char *);

int forsPAFSetValueBool(ForsPAF *, const char *, int);
int forsPAFSetValueInt(ForsPAF *, const char *, int);
int forsPAFSetValueDouble(ForsPAF *, const char *, double);
int forsPAFSetValueString(ForsPAF *, const char *, const char *);
int forsPAFSetComment(ForsPAF *, const char *, const char *);

/*
 * Inserting and removing elements
 */

int forsPAFInsertBool(ForsPAF *, const char *, const char *, int, const char *);
int forsPAFInsertInt(ForsPAF *, const char *, const char *, int, const char *);
int forsPAFInsertDouble(ForsPAF *, const char *, const char *, double,
                       const char *);
int forsPAFInsertString(ForsPAF *, const char *, const char *, const char *,
                       const char *);

int forsPAFInsertAfterBool(ForsPAF *, const char *, const char *, int,
                          const char *);
int forsPAFInsertAfterInt(ForsPAF *, const char *, const char *, int,
                         const char *);
int forsPAFInsertAfterDouble(ForsPAF *, const char *, const char *, double,
                            const char *);
int forsPAFInsertAfterString(ForsPAF *, const char *, const char *,
                            const char *, const char *);

int forsPAFPrependBool(ForsPAF *, const char *, int, const char *);
int forsPAFPrependInt(ForsPAF *, const char *, int, const char *);
int forsPAFPrependDouble(ForsPAF *, const char *, double, const char *);
int forsPAFPrependString(ForsPAF *, const char *, const char *, const char *);

int forsPAFAppendBool(ForsPAF *, const char *, int, const char *);
int forsPAFAppendInt(ForsPAF *, const char *, int, const char *);
int forsPAFAppendDouble(ForsPAF *, const char *, double, const char *);
int forsPAFAppendString(ForsPAF *, const char *, const char *, const char *);

void forsPAFErase(ForsPAF *, const char *);
void forsPAFClear(ForsPAF *);

/*
 * Read and write operations
 */

int forsPAFWrite(ForsPAF *);

/*
 * Miscellaneous utilities
 */

int forsPAFIsValidName(const char *);

CPL_END_DECLS

#endif /* FORS_PAF_H */
