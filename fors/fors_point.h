/* $Id: fors_point.h,v 1.4 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_POINT_H
#define FORS_POINT_H

#include <cpl.h>

CPL_BEGIN_DECLS

typedef struct _fors_point
{
    double x, y;

} fors_point;

#undef LIST_ELEM
#define LIST_ELEM fors_point
#include <list.h>

fors_point *fors_point_new(double x, double y);
fors_point *fors_point_duplicate(const fors_point *p);
void fors_point_delete(fors_point **point);
void fors_point_delete_list(fors_point ***point);
void fors_point_print(const fors_point *s);
bool fors_point_equal(const fors_point *p,
		      const fors_point *q);

double fors_point_distsq(const fors_point *p,
			 const fors_point *q);


CPL_END_DECLS

#endif
