/* $Id: fors_pfits.h,v 1.12 2011-10-24 13:08:17 cgarcia Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2011-10-24 13:08:17 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_PFITS_H
#define FORS_PFITS_H

#include <cpl.h>

CPL_BEGIN_DECLS

extern const char *const FORS_PFITS_INSTRUME;
extern const char *const FORS_PFITS_FILTER_NAME;
extern const char *const FORS_PFITS_EXPOSURE_TIME;
extern const char *const FORS_PFITS_AIRMASS_START;
extern const char *const FORS_PFITS_AIRMASS_END;
extern const char *const FORS_PFITS_OUTPUTS;
extern const char *const FORS_PFITS_CONAD[4];
extern const char *const FORS_PFITS_RON[4];
extern const char *const FORS_PFITS_OVERSCANX;
extern const char *const FORS_PFITS_OVERSCANY;
extern const char *const FORS_PFITS_PRESCANX;
extern const char *const FORS_PFITS_PRESCANY;
extern const char *const FORS_PFITS_CRPIX1;
extern const char *const FORS_PFITS_CRPIX2;
extern const char *const FORS_PFITS_DET_NX;
extern const char *const FORS_PFITS_DET_NY;
extern const char *const FORS_PFITS_BINX;
extern const char *const FORS_PFITS_BINY;
extern const char *const FORS_PFITS_PIXSCALE;
extern const char *const FORS_PFITS_READ_CLOCK;
extern const char *const FORS_PFITS_CHIP_ID;
extern const char *const FORS_PFITS_TARG_NAME;

CPL_END_DECLS

#endif
