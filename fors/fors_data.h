/* $Id: fors_data.h,v 1.16 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.16 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_DATA_H
#define FORS_DATA_H

#include <fors_std_star.h>
#include <fors_setting.h>

#include <cpl.h>

/* Photometry table column names */
extern const char *const FORS_DATA_PHOT_FILTER;
extern const char *const FORS_DATA_PHOT_EXTCOEFF;
extern const char *const FORS_DATA_PHOT_DEXTCOEFF;
extern const char *const FORS_DATA_PHOT_ZEROPOINT;
extern const char *const FORS_DATA_PHOT_DZEROPOINT;
extern const char *const FORS_DATA_PHOT_COLORTERM;
extern const char *const FORS_DATA_PHOT_DCOLORTERM;

CPL_BEGIN_DECLS

void
fors_std_star_list_apply_wcs(               fors_std_star_list      *stars,
                                            const cpl_propertylist  *header);

void fors_phot_table_load(const cpl_frame *phot_table_frame,
			  const fors_setting *setting,
			  double *color_term,
			  double *dcolor_term,
			  double *ext_coeff,
			  double *dext_coeff,
			  double *expected_zeropoint,
			  double *dexpected_zeropoint);

cpl_table *fors_phot_coeff_create(const fors_setting *,
                                  double color_term,
                                  double dcolor_term,
                                  double ext_coeff,
                                  double dext_coeff,
                                  double zeropoint,
                                  double dzeropoint);

CPL_END_DECLS
#endif
