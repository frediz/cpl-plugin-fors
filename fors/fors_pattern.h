/* $Id: fors_pattern.h,v 1.3 2010-09-14 07:49:30 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-09-14 07:49:30 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef FORS_PATTERN_H
#define FORS_PATTERN_H

#include <fors_point.h>

#include <stdbool.h>

typedef struct _fors_pattern fors_pattern;

#undef LIST_ELEM
#define LIST_ELEM fors_pattern
#include <list.h>

fors_pattern *fors_pattern_new(const fors_point *ref,
                               const fors_point *p1,
                               const fors_point *p2,
                               double sigma);

fors_pattern_list *
fors_pattern_new_from_points(struct fors_point_list *points,
			     double tolerance,
			     double sigma);

void
fors_pattern_delete(fors_pattern **p);

void fors_pattern_print(const fors_pattern *p);

const fors_point *
fors_pattern_get_ref(const fors_pattern *p);

double fors_pattern_get_scale(const fors_pattern *p,
                              const fors_pattern *q);
double fors_pattern_get_angle(const fors_pattern *p,
                              const fors_pattern *q);

void fors_pattern_get_shift(const fors_pattern *p,
			    const fors_pattern *q,
			    double *shift_x,
			    double *shift_y);

double fors_pattern_distsq(const fors_pattern *p,
                           const fors_pattern *q);

double fors_pattern_dist_per_error(const fors_pattern *p,
				   const fors_pattern *q);

void fors_pattern_error(const fors_pattern *p,
			double *dr2,
			double *dtheta);

#endif
